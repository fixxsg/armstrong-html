<?php
    if (isset($_GET["id"])) $id = $_GET["id"];
    else                    $id = null;

    $page = "investment";
    include ('header.php');
?>
        <div id="wrap-container" class="container">
            <!-- <div class="row"> -->
                <div id="wrap-sidebar" class="col-xs-12 standard-wid">
                    <div id="sidebar">

                        <?php 
                            if($id == null || $id == 1 || $id > 4)
                                echo '<div class="business item" data-id="#item-content-1">';
                            else 
                                echo '<div class="business item faded" data-id="#item-content-1">';
                        ?>
                            <div class="stitle">INVESTMENT PORTFOLIO</div>
                            <div class="bdescription">
                                Armstrong S.E. Asia Clean Energy Fund
                            </div>
                            <a href="#" class="sviewmore"></a>
                        </div>

                        <div class="business item <?php if($id != 2) echo 'faded'?>" data-id="#item-content-2">
                            <img src="images/lg_symbior_energy.png" alt="Symbior Energy">
                            <div class="bdescription">
                                Established in 2010, Symbior Solar Siam (SSS) is a graduate of Symbior Energy, the Hong Kong-based energy venture incubator. 
                            </div>
                            <a href="#" class="sviewmore"></a>
                        </div>

                        <div class="business item <?php if($id != 3) echo 'faded'?>" data-id="#item-content-3">
                            <img src="images/lg_annex_power.png" alt="Annex Power">
                            <div class="bdescription">
                                Annex Power Ltd is an highly experienced renewable energy group focused on Southeast Asia.
                            </div>
                            <a href="#" class="sviewmore"></a>
                        </div>

                        <div class="business item <?php if($id != 4) echo 'faded'?>" data-id="#item-content-4">
                            <img src="images/lg_the_blue_circle.png" alt="The Blue Circle">
                            <div class="bdescription">
                                The Blue Circle focuses on developing wind and solar energy projects in Thailand, Vietnam and Cambodia.
                            </div>
                            <a href="#" class="sviewmore"></a>
                        </div>
                    </div>
                </div>
            <!-- </div>
            <div class="row"> -->
                <div id="wrap-content">
                    <div id="content-cover" class="col-md-12 col-xs-12 content-cover">

                        <?php 
                            if($id == null || $id == 1 || $id > 4)
                                echo '<div class="item-content" id="item-content-1">';
                            else 
                                echo '<div class="item-content hidden" id="item-content-1">';
                        ?>
                            <h1>Armstrong S.E. Asia Clean Energy Fund</h1>
                            <p>
                                The Armstrong S.E. Asia Clean Energy Fund is a private equity fund that invests in small-scale renewable energy and resource efficiency projects in Southeast Asia. This strategy is driven by the high energy demand and strong market fundamentals in the region. Armstrong Asset Management will seek to provide investors with a gross return in excess of 20% per annum.
                            </p><p>
                                The geographic focus of the Armstrong S.E. Asia Clean Energy Fund is South East Asia, focusing particularly on Thailand, Philippines, Indonesia and Vietnam.
                            </p><p>
                                The investment strategy is based on the key features summarised below:
                                <ul>
                                    <li>An obvious market need supported by strong economic fundamentals</li>
                                    <li>A commitment to positive social and environmental impact</li>
                                    <li>Risk minimisation through a portfolio of small-scale projects</li>
                                    <li>No technology risk</li>
                                    <li>The ability to generate early cash flows</li>
                                    <li>Excellent entry valuations due to lack of investor competition</li>
                                    <li>Competitive advantage as a result of the team’s local operating experience</li>
                                    <li>A clear exit strategy</li>
                                </ul>
                            </p>
                            
                            <hr class="clearfix" style="box-sizing:content-box;"/>

                            <h2>INVESTMENT FOCUS</h2>
                            <p>
                                The Armstrong S.E. Asia Clean Energy Fund is focused predominantly but not exclusively on the following areas of investment:
                            </p>
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <h5>Sector focus</h5>
                                <p>Small-scale renewable energy generation – wind, solar, hydro, biomass, waste to energy Resource efficiency – clean water supply, waste recycling, energy efficiency</p>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <h5>Geographic focus</h5>
                                <p>South-East Asia, with focus on Indonesia, Philippines, Malaysia, Thailand and Vietnam</p>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <h5>Investment stage</h5>
                                <p>Development (Pre-Permitting)<br/>
                                Green Field (Post-Permitting)<br/>
                                Operating Assets</p>
                            </div>

                            <!-- <hr class="clearfix" style="box-sizing:content-box;"/>
                            <div class="col-sm-12 col-md-12 col-lg-12" style="background:#fff;">
                                <br/>
                                <img width="80%" src="images/investors/ifc.png"><br/><br/>
                                <img width="80%" src="images/investors/amc.png"><br/><br/>
                                <img width="100%" src="images/investors/flags.png"><br/>
                                <img width="80%" src="images/investors/obivam.png"><br/><br/>
                                <img width="80%" src="images/investors/unigestion.png"><br/>
                                &nbsp;
                            </div> -->
                        </div>

                        <div class="item-content <?php if($id != 2) echo 'hidden'?>" id="item-content-2">
                            <h1>SYMBIOR SOLAR SIAM</h1>
                            <h5><a href="http://www.armstrongam.com/news/62-armstrong-solar-deal-symbior">Press Release</a></h5>
                            <p class="img-cover">
                                <img width="100%" src="images/investment/symbior_solar_siam.jpg">
                            </p>
                            <table class="table-ins hidden-xs">
                                <tr>
                                    <td><strong>Date of Investment: </strong>September 2013</td>
                                    <td><strong>Investment Status: </strong>Current</td>
                                </tr>
                                <tr>
                                    <td><strong>Geography: </strong>Thailand</td>
                                    <td><strong>Technologies: </strong>Solar PV</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><strong>Website: </strong><a href="http://www.symbiorenergy.com/index.php/ses/symbior-solar-siam/">www.symbiorenergy.com</a></td>
                                </tr>
                            </table>
                            <p>
                                Established in 2010, Symbior Solar Siam (SSS) is a graduate of Symbior Energy, the Hong Kong-based energy venture incubator. SSS has been developing a distributed energy generation platform in rural Thailand using solar PV technology which now includes a pipeline of projects and implementation plans to deploy 200MW of solar PV generative capacity in the Kingdom by 2015. True to Symbior Energy’s Sustainable Energy Solutions concepts, the company’s small-scale solar PV power plants provide rural regions of Thailand with clean energy supplies which support sustainable and equitable growth.
                            </p>
                        </div>

                        <div class="item-content <?php if($id != 3) echo 'hidden'?>" id="item-content-3">
                            <h1>ANNEX POWER</h1>
                            <h5><a href="http://www.armstrongam.com/news/64-armstrong-annex">Press Release</a></h5>
                            <p>
                                <iframe width="100%" height="400px" src="http://www.youtube.com/embed/mNrZWRLbU5k" mce_src="http://www.youtube.com/embed/mNrZWRLbU5k" frameborder="0" allowfullscreen="yes"></iframe>
                            </p>
                            <table class="table-ins hidden-xs">
                                <tr>
                                    <td><strong>Date of Investment: </strong>September 2014</td>
                                    <td><strong>Investment Status: </strong>Current</td>
                                </tr>
                                <tr>
                                    <td><strong>Geography: </strong>ASEAN Region with a focus on Indonesia, Philippines, Thailand</td>
                                    <td><strong>Technologies: </strong>Biogas, Solar PV, Wind</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><strong>Website: </strong><a href="http://www.annexpower.com">www.annexpower.com</a></td>
                                </tr>
                            </table>
                            <p>
                                Annex Power Ltd is an highly experienced renewable energy group focused on Southeast Asia. With over 80 professionals experienced in the engineering and construction of solar, biogas and wind power plants, Annex has planned, financed, built and operated more than 100 MW of renewable energy projects. Annex insists upon high quality products, components, and engineering standards to help generate clean and efficient energy for homes, businesses, governments and utilities in the region. Annex’s philosophy is to offer clients and partners independent solutions that are not only reliable and value-driven, but that also generate the greatest positive environmental and social impacts.
                            </p>
                        </div>

                        <div class="item-content <?php if($id != 4) echo 'hidden'?>" id="item-content-4">
                            <h1>THE BLUE CIRCLE</h1>
                            <h5><a href="http://www.armstrongam.com/news/90-aam-blue-circle">Press Release</a></h5>
                            <p class="img-cover">
                                <img width="100%" src="images/investment/the_blue_circle.jpg">
                            </p>
                            <table class="table-ins hidden-xs">
                                <tr>
                                    <td><strong>Date of Investment: </strong>May 2014</td>
                                    <td><strong>Investment Status: </strong>Current</td>
                                </tr>
                                <tr>
                                    <td><strong>Geography: </strong>Thailand, Vietnam, Cambodia</td>
                                    <td><strong>Technologies: </strong>Wind and Solar PV</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><strong>Website: </strong><a href="http://www.thebluecircle.sg">www.thebluecircle.sg</a></td>
                                </tr>
                            </table>
                            <p>
                                The Blue Circle focuses on developing wind and solar energy projects in Thailand, Vietnam and Cambodia. The Singapore based company looks to bridge the gap in project development in the Mekong Region by bringing international project development experience, financial expertise and capabilities together with local market understanding. Its growth strategy is twofold: through the development of its own projects and through acquisition or partnership with local developers. By being vertically integrated, The Blue Circle can identify Greenfield sites; pursue project development milestones up until financing and operating of the generating assets.
                            </p>
                        </div>

                        <span class="content-close" id="content-close" onclick="content_close()"></span>
                    </div>
                </div>
            <!-- </div> -->
            <div class="clearfix"></div>
        </div>
    
    
    <?php
        include ('footer.php');
    ?>

    <!-- Jquery -->
    <script src="js/armstrongam.js"></script>

    </body>
</html>
