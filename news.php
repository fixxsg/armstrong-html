<?php
    if (isset($_GET["id"])) $id = $_GET["id"];
    else                    $id = null;

    $page = "news";
    include ('header.php');
?>
        
        <div id="wrap-container" class="container">
            <div id="wrap-sidebar" class=" col-xs-12 standard-wid">
                <div id="sidebar">
                    <div class="investment_portfolio item news">
                        <div class="stitle">Latest New</div>
                    </div>

                    <?php 
                        if($id == null || $id == 1 || $id > 5)
                            echo '<div class="business item news" data-id="#item-content-1">';
                        else 
                            echo '<div class="business item news faded" data-id="#item-content-1">';
                    ?>
                        <p class="date">08 August 2014</p>
                        <p class="title">
                            Armstrong Teams Up With nv vogt to Replace Diesel Generation with Solar Power Plants in the Philippines
                        </p>
                        <p class="describe">
                            <strong>Singapore - August 8, 2014</strong> - Armstrong Asset Management (AAM) has committed to provide US$29 million to fund the construction of a pipeline of bilateral solar power projects in the Philippines being developed by the development company, nv vogt Singapore.
                        </p>
                    </div>

                    <div class="business item news <?php if($id != 2) echo 'faded'?>" data-id="#item-content-2">
                        <p class="date">21 July 2014</p>
                        <p class="title">
                            Armstrong Enters Mini-Hydro Market in Indonesia with NKE Group
                        </p>
                        <p class="describe">
                            <strong>Singapore - July 21st, 2014</strong> - Armstrong Asset Management has agreed to commit up to US $22.5 million to develop and operate a portfolio of 50MW mini-hydro plants in Indonesia.
                        </p>
                    </div>

                    <div class="business item news <?php if($id != 3) echo 'faded'?>" data-id="#item-content-3">
                        <p class="date">15 July 2014</p>
                        <p class="title">
                            Armstrong Wins Best Launch of an Infrastructure Fund at the AsianInvestor Awards
                        </p>
                        <p class="describe">
                            <strong>Singapore - July 9th, 2014</strong> - The Blue Circle, a renewable energy developer operating in the Mekong region (Vietnam, Thailand and Cambodia) and based in Singapore, announced a partnership with Annex Power, a leading renewable energy group in Southeast Asia, to jointly develop wind power plants in Thailand.
                        </p>
                    </div>

                    <div class="business item news <?php if($id != 4) echo 'faded'?>" data-id="#item-content-4">
                        <p class="date">09 July 2014</p>
                        <p class="title">
                            Armstrong Wins Best Launch of an Infrastructure Fund at the AsianInvestor Awards
                        </p>
                        <p class="describe">
                            Armstrong Asset Management and the Armstrong South East Asia Clean Energy Fund won the AsianInvestor, Investment Performance award for Best Launch of an Infrastructure Fund. To view the rest of the award winners, please click here.
                        </p>
                    </div>

                    <div class="business item news <?php if($id != 5) echo 'faded'?>" data-id="#item-content-5">
                        <p class="date">19 May 2014</p>
                        <p class="title">
                            19 May 2014 - CMIA Publishes New Report on Assessing Renewable Energy Regimes in South East Asia
                        </p>
                        <p class="describe">
                            The Climate Markets & Investment Association is pleased to share its new report assessing renewable energy regimes in Indonesia, Thailand and the Philippines. The report was launched on Monday 19th May in Manila, during an event that gathered approximately 50 people. Please download the full report </span><a href="http://www.cmia.net/press-releases/141-cmia-report-renewable-energy-in-asia-140519" mce_href="http://www.cmia.net/press-releases/141-cmia-report-renewable-energy-in-asia-140519" target="_blank" style="text-align: justify;" mce_style="text-align: justify;">here</a>.
                        </p>
                    </div>
                </div>
            </div>
            
            <!--  -->

            <div id="wrap-content">
                <div id="content-cover" class="col-md-12 content-cover">
                    <?php 
                        if($id == null || $id == 1 || $id > 5)
                            echo '<div class="item-content" id="item-content-1">';
                        else 
                            echo '<div class="item-content hidden" id="item-content-1">';
                    ?>
                        <h1>Armstrong Teams Up With nv vogt to Replace Diesel Generation with Solar Power Plants in the Philippines</h1>
                        <p>
                            <b>Singapore - August 8, 2014 - </b>Armstrong Asset Management (AAM) has committed to provide US$29 million to fund the construction of a pipeline of bilateral solar power projects in the Philippines being developed by the development company, nv vogt Singapore.</p>
                            <p>The first project is a 6.25 MW ground-mounted and grid-connected solar power plant in South Cotabato in the southern region of Mindanao, which is scheduled to begin construction this month (August 2014) and is expected to commence commercial operations by December this year.&nbsp;</p>
                            <p>The bilateral Power Purchase Agreement (PPA) was executed with the off-taker, South Cotabato I Electric Cooperative (“SOCOTECO I”), an “A+” rated cooperative on 26 June, 2013.</p>
                            <p>The South Cotabato 6.25 MW project will be the world’s largest diesel replacement power plant when it is completed, given that 100% of its generated power will replace diesel-generated peak power.</p>
                            <p>Singapore-based Managing Partner of AAM, Andrew Affleck said this investment, the first by the Fund in the Philippines and the first with nv vogt, represents a benchmark for the development of diesel-offset projects globally and one which does not rely on subsidies by way of feed-in-tariffs.</p>
                            <p>“With the projected power deficit forecast for 2015 in the Philippines, this project can serve as a benchmark for clean and quick decentralized energy solutions without the need for government subsidies. Armstrong is very proud to be funding this precedent setting project.” Mr Affleck said.</p>
                            <p>Anton Milner, Managing Director of the solar power plant business for the Berlin-based owner ib vogt and Chairman of nv vogt Singapore, said he was pleased to be working with AAM as the Fund obviously “shares our bullish view” of South East Asia as the ideal region to develop economically viable renewable energy projects.</p>
                            <p>"We believe solar power is a compelling need as well as attractive opportunity in the developing markets of Asia. Our first project in the Philippines is doubly important because it will provide clean and unsubsidized solar photo voltaic (PV) electricity benefitting the wider community and it is one of the largest diesel-offset projects in the world to date, which is an important future market,” Mr Milner said.</p>
                            <p>Vivek Chaudhri, President of nv vogt Philippines added that the partnership with Armstrong “will fund several projects in our Philippines pipeline, allowing us to focus on what we do best - develop and realise utility scale PV projects."</p>
                            <p>The exploration of Philippines projects began in 2010 when two of the founders of APCA Power, a development company based in India, decided to look outside of India for solar projects. After reviewing the opportunities with their partner, Germany based ib vogt GmbH, the joint venture development company nv vogt was established in Singapore for the specific purpose of developing projects in the Philippines and SE Asia. It has spent the last 36 months building up a presence and a pipeline in the Philippines, initiating Project 1 in October 2012 with the signing of the PPA in June 2013.&nbsp;</p>
                            <p><b>About Armstrong Asset Management</b></p>
                            <p>Armstrong Asset Management is an independent asset manager, based in Singapore, focused on the clean energy sector in South East Asia’s emerging markets. Armstrong invests in small-scale infrastructure projects and achieved a final close on its debut clean energy fund of US$164m in November 2013, with institutional investors such as IFC, DEG, FMO, Proparco, SIFEM, GEEREF and Unigestion. &nbsp;Operating with a multidisciplinary team of investment professionals, all of whom possess deep sector knowledge and a collective 80 years of South East Asia operating experience, Armstrong Asset Management integrates strict environmental, social and governance compliance into its investment process to deliver tangible benefits and reduce risks for all of its stakeholders. <a href="http://www.armstrongam.com" mce_href="http://www.armstrongam.com">www.armstrongam.com</a></p>
                            <p><b>About nv vogt</b></p>
                            <div>nv vogt Singapore Pte Ltd is focusing on developing, designing, financing, constructing and operating solar power plants in Asia. The founders of nv vogt are pioneers of the solar industry with extensive experience in constructing and operating solar power plants in Europe and Asia. The primary customer for nv vogt is the energy-intensive industry that is being underserved by the grid and is heavily reliant on diesel power, investing in its own projects as well as offering a flexible model for coinvestors. Its initial focus is on opportunities in India and the Philippines, where it has fully owned subsidiaries, while over time, it plans to expand to additional countries in Asia. The company’s objective is to develop a portfolio of projects that combine excellent engineering with financial optimisation, in order to generate superior returns, both for customers and investors. Technologically, its projects are optimised to minimise LCOE (levelised cost of energy) while maintaining long -term reliability. Financially, its projects are structured to ensure bankability and investment-grade returns. ib vogt GmbH is a 40% owner of the Singapore-based nv vogt. &nbsp;<a href="http://www.nv-vogt.com" mce_href="http://www.nv-vogt.com">www.nv-vogt.com</a></div>
                            <p><br/></p>
                            <p><b>Issued by H2PC Asia on behalf of Armstrong Asset Management</b></p>
                            <p>For further information, please contact:</p>
                            <p>H2PC Asia | Ken Hickson | +65 8139 7472 | <a href="mailto:kenhickson@h2pcasia.com" mce_href="mailto:kenhickson@h2pcasia.com">kenhickson@h2pcasia.com</a></p>
                            <p>Andrew Affleck | Tel: +65 66229790 | <a href="mailto:andrew.affleck@armstrongam.com" mce_href="mailto:andrew.affleck@armstrongam.com">andrew.affleck@armstrongam.com</a></p>
                            <p>nv vogt Singapore Pte Ltd | Tel: +65 6334 9277 | <a href="mailto:deepak.verma@nv-vogt.com" mce_href="mailto:deepak.verma@nv-vogt.com">deepak.verma@nv-vogt.com</a></p>
                            <p>ib vogt GmbH | Tel: + 49 30 397 4400 | <a href="mailto:anton.milner@ibvogt.com" mce_href="mailto:anton.milner@ibvogt.com">anton.milner@ibvogt.com</a></p>
                        </p>
                    </div>

                    <div class="item-content <?php if($id != 2) echo 'hidden'?>" id="item-content-2">
                        <h1>Armstrong Enters Mini-Hydro Market in Indonesia with NKE Group</h1>
                        <p>
                            <b>Singapore - July 21st, 2014 - </b>Armstrong Asset Management has agreed to commit up to US $22.5 million to develop and operate a portfolio of 50MW mini-hydro plants in Indonesia.</p>
                            <p>Armstrong’s South East Asia Clean Energy Fund has agreed to fund the construction of a portfolio of mini-hydro power generation projects in Indonesia being developed by PT Inti Duta Energi (IDE), the mini-hydro development subsidiary of Jakarta-listed construction company PT Nusa Konstruksi Enjiniring Tbk (NKE).</p>
                            <p>Currently, the developer has a pipeline of well over 50MW mini-hydro projects in various stage of permitting process. The first project of 5MW output located in Java is expected to be operational by the second quarter of 2016, followed by additional three projects totalling 21MW output located in Sumatra by the end of 2016.</p>
                            <p>Michael McNeill, Partner of Singapore-based Armstrong Asset Management said: “We are very pleased to announce this partnership with NKE Group. We look forward to building on our excellent working relationship with the IDE team whose project development and technical capabilities are well recognized in mini-hydro market in Indonesia. Together with NKE group’s proven strong project management, engineering and construction capabilities, we expect to deploy our capital rapidly to deliver high-quality mini-hydro assets to support sustainable development of Indonesia.”</p>
                            <p>Djohan Halim, President Director of PT Inti Duta Energi welcomed the partnership and investment plan. “It is great to establish this synergy with Armstrong Asset Management. We share a commitment to the potential and the need for rapid growth in the development of clean, efficient and low risk energy solutions. The experience of the Armstrong team in small-scale infrastructure investment in South East Asia is a powerful complement to our activities in Indonesia.”</p>
                            <p>Armstrong closed its Clean Energy Fund when it reached US164 million last November. Prior to the latest deal, it has made three investments from the fund: a capital commitment of up to US$30 million to Annex Power for solar and biogas projects in Thailand, Indonesia and the Philippines; an equity stake in Symbior Elements to develop a portfolio of solar generation in Central and Northeast Thailand, and a capital commitment of up to US$ 40 million to The Blue Circle for wind and solar projects in Mekong Region.</p>
                            <p>Indonesia has the largest renewable energy resources in South East Asia with sustained long term growth in power demand. After the revision of feed-in-tariff system announced recently, the Indonesian mini-hydro has now become one of the most attractive renewable energy sectors in South East Asia. &nbsp;</p>
                            <p>In April this year, Armstrong also announced a partnership with Mandiri Investment Management to work together to invest in renewable energy projects, starting from mini-hydro sector in Indonesia.&nbsp;</p>
                            <p><b>About Armstrong Asset Management</b></p>
                            <p>Armstrong Asset Management is an independent asset manager, based in Singapore, focused on the clean energy sector in South East Asia’s emerging markets. Armstrong invests in small-scale infrastructure projects and achieved a final close on its debut clean energy fund of US$164m in November 2013, with institutional investors such as IFC, DEG, FMO, Proparco, SIFEM, GEEREF and Unigestion. &nbsp;Operating with a multidisciplinary team of investment professionals, all of whom possess deep sector knowledge and a collective 80 years of South East Asia operating experience, Armstrong Asset Management integrates strict environmental, social and governance compliance into its investment process to deliver tangible benefits and reduce risks for all of its stakeholders. www.armstrongam.com</p>
                            <p><b>About PT Nusa Konstruksi Enjiniring (NKE) and PT Inti Duta Energy (IDE)</b></p>
                            <p>PT Nusa Konstruksi Enjiniring (NKE) is an Indonesian public listed company with construction and engineering as its core business. &nbsp;The company capabilities cover both civil and building works. Since the establishment in 1982, NKE has successfully completed over 200 infrastructure projects and over 250 building projects across the Indonesian Archipelago, including the construction of 180 MW Asahan 1 Hydro-Electric power plant in Sumatera. With 30 years of experience NKE has built strong professional network and capable personnel. NKE has a unique approach to overcome complex engineering problems, which have attributed to its reputation and proven capabilities as a contractor. Since 2011 NKE has incorporated a full subsidiary company, PT Inti Duta Energy (IDE), which is focused on obtaining concessions for Mini-Hydro-Power facilities, building and developing these facilities and operating these facilities. www.nusakonstruksi.com</p>
                            <p><b>Issued by H2PC Asia on behalf of Armstrong Asset Management</b></p>
                            <p>For further information, please contact:</p>
                            <p>H2PC Asia | Ken Hickson | +65 8139 7472 | kenhickson@h2pcasia.com</p>
                            <p>Armstrong Asset Management | Michael McNeill | +65 9666 5313 | michael.mcneill@armstrongam.com</p>
                            <p>Nusa Konstruksi Enjiniring / Inti Duta Energi | Almanda Pohan | +62 81219678141 | almanda.pohan@nusakonstruksi.com</p>
                        </p>
                    </div>

                    <div class="item-content <?php if($id != 3) echo 'hidden'?>" id="item-content-3">
                        <h1>The Blue Circle partners with Annex Power to Develop Wind Energy in Thailand</h1>
                        <p>
                            <b>Singapore - July 9th, 2014 - </b>The Blue Circle, a renewable energy developer operating in the Mekong region (Vietnam, Thailand and Cambodia) and based in Singapore, announced a partnership with Annex Power, a leading renewable energy group in Southeast Asia, to jointly develop wind power plants in Thailand.</p>
                            <p>Annex Power, based in Bangkok, is a preeminent renewable energy Engineering, Procurement and Construction (EPC) and project development company in Thailand. Annex Power has been a pioneer in the large-scale solar sector in Thailand with more than 70 MW installed and commissioned. The company has also been involved in the wind sector in Thailand for three years, and has installed and monitored wind measuring equipment in various locations throughout the country. Annex Power brings its engineering experience as well as its grid connection and construction expertise.</p>
                            <p>Under the partnership, both Annex Power and The Blue Circle agree to commit resources to develop wind energy projects in Thailand. Daniel Gaefke, Managing Director of Annex Power, is keen to participate in the future growth of wind power in Thailand: “Combining The Blue Circle’s wind experience in Europe, North America, South Africa and Australia with Annex Power’s track record in solar, hybrid systems and biogas will create a powerful partnership to successfully develop projects in the country”.</p>
                            <p>With an installed capacity of 223 MW on 3 different projects, wind power in Thailand is poised for growth in the coming years. “The Blue Circle is proud to partner with such an established group in renewable energy in Thailand as Annex Power, and is committed to be a significant player in the wind sector in Thailand. The current situation is similar to France in 2002, just prior to the wind market taking off from installation of 100 MW a year to 1,000 MW,” said Olivier Duguet, Chief Executive Officer for The Blue Circle, former CEO of the largest wind Independent Power Producer in France. By focusing primarily on small projects, the partnership intends to unlock potential sites all over Thailand and to implement state-of-the-art technologies to assess and harvest the low wind speeds found in Thailand.&nbsp;</p>
                            <p><b>About Annex Power</b></p>
                            <p>Annex Power is an experienced and proven renewable energy group focused on Southeast Asia. With over 100 professionals experienced in the engineering and construction of solar, biogas and wind power plants, Annex has developed, planned, financed, built and operated more than 130 MW of renewable energy projects. Annex utilizes high quality products, components and engineering standards to help generate clean and efficient energy for homes, businesses, governments and utilities in the region. Annex’s philosophy is to offer clients and partners independent solutions that are not only reliable and value-driven, but that also generate the greatest environmental and community impacts.&nbsp;<a href="http://www.annexpower.com" mce_href="http://www.annexpower.com" target="_blank">www.annexpower.com</a></p>
                            <p><b>About The Blue Circle</b></p>
                            <p>The Blue Circle is a developer of wind and solar energy projects in Thailand, Vietnam and Cambodia. The Singapore-based company looks to bridge the gap in project development in the Mekong Region by bringing international project development experience, financial expertise and capabilities together with local market understanding. Its growth strategy is twofold: through the development of its own projects and through acquisition or partnership with local developers. By being vertically integrated, The Blue Circle can identify greenfield sites, pursue project development milestones up until financing and operating of the generating assets.&nbsp;<a href="http://www.thebluecircle.sg" mce_href="http://www.thebluecircle.sg" target="_blank">www.thebluecircle.sg</a></p>
                            <p><b>For further information, please contact:</b></p>
                            <p>The Blue Circle | Olivier Duguet | Tel: +65 62594921 | olivier.duguet@thebluecircle.sg</p>
                            <p>Annex Power | Daniel Gaefke | Tel: +66 26606801 | daniel@annexpower.com</p>
                        </p>
                    </div>

                    <div class="item-content <?php if($id != 4) echo 'hidden'?>" id="item-content-4">
                        <h1>Armstrong Wins Best Launch of an Infrastructure Fund at the AsianInvestor Awards</h1>
                        <p>
                            <p><span mce_style="text-align: justify;" style="text-align: justify;">Armstrong Asset Management and the Armstrong South East Asia Clean Energy Fund won the AsianInvestor, Investment Performance award for Best Launch of an Infrastructure Fund. To view the rest of the award winners, please click&nbsp;</span><a href="http://www.asianinvestor.net/News/382661,investment-performance-awards-2014-day-1.aspx" mce_href="http://www.asianinvestor.net/News/382661,investment-performance-awards-2014-day-1.aspx" target="_blank" mce_style="text-align: justify;" style="text-align: justify;">here</a><span mce_style="text-align: justify;" style="text-align: justify;">.</span></p>
                            <p><b><img mce_style="vertical-align: middle;" style="vertical-align: middle;" mce_src="http://armstrongam.com/images/stories/aiawards-307.jpg" src="http://armstrongam.com/images/stories/aiawards-307.jpg" width="450" height="300"></b></p><p><br/></p><p class="MsoNormal" mce_style="text-align:justify" style="text-align: justify;"><b></b></p>
                        </p>
                    </div>

                    <div class="item-content <?php if($id != 5) echo 'hidden'?>" id="item-content-5">
                        <h1>19 May 2014 - CMIA Publishes New Report on Assessing Renewable Energy Regimes in South East Asia</h1>
                        <p>
                            <p><span style="text-align: justify;" mce_style="text-align: justify;">The Climate Markets &amp; Investment Association is pleased to share its new report assessing renewable energy regimes in Indonesia, Thailand and the Philippines. The report was launched on Monday 19th May in Manila, during an event that gathered approximately 50 people. Please download the full report </span><a href="http://www.cmia.net/press-releases/141-cmia-report-renewable-energy-in-asia-140519" mce_href="http://www.cmia.net/press-releases/141-cmia-report-renewable-energy-in-asia-140519" target="_blank" style="text-align: justify;" mce_style="text-align: justify;">here</a><span style="text-align: justify;" mce_style="text-align: justify;">.</span></p>
                            <p class="MsoNormal" style="text-align:justify" mce_style="text-align:justify"><br/></p><p class="MsoNormal" mce_style="text-align:justify" style="text-align: justify;"><b></b></p>
                        </p>
                    </div>

                    <span class="content-close" id="content-close" onclick="content_close()"></span>
                </div><!-- End Content Cover -->
            </div><!-- End Wrap-Content -->

            <!-- <div class="clearfix"></div> -->
        </div>

   <?php
        include ('footer.php');
    ?>

    <!-- Jquery -->
    <script src="js/armstrongam.js"></script>

    </body>
</html>

