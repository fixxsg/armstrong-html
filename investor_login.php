<?php
$page = "legal";
include ('header.php');
?>

		<div id="wrap-container">
			<div id="wrap-sidebar">
                <div class="item-content" id="item-content-1">
                    <h1>INVESTOR LOGIN</h1>

                    <!-- This was copied directly from original form -->
                    <form action="/cookies" method="post" name="login" id="form-login">
						<div id="form-login-username">
							<label for="modlgn_username">Username</label>
							<input id="modlgn_username" type="text" name="username" class="inputbox" alt="username" size="18"/>
						</div>

						<div id="form-login-password">
							<label for="modlgn_passwd">Password</label>
							<input id="modlgn_passwd" type="password" name="passwd" class="inputbox" size="18" alt="password"/>
						</div>
						 
						<input type="submit" name="Submit" class="button" value="Login"/>
						<a href="javascript:;" onclick="this.blur();showThem('login_pop');return false;" id="openLogin">
						Forgot your password?</a>
						
						<input type="hidden" name="option" value="com_user"/>
						<input type="hidden" name="task" value="login"/>
						<input type="hidden" name="return" value="L2ludmVzdG9yLWFyZWE="/>
						<input type="hidden" name="a7187d5fc16acf2af51915298a965d67" value="1"/>
					</form>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

<?php
include ('footer.php');
?>

<!-- script -->
<script type="text/javascript" src="js/sidebar-content.js"></script>