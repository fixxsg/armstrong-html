// Window load event used just in case window height is dependant upon images
$(window).bind("load", function() {

    var footerHeight = 0,
        $footer = $(".footer-sticky");

    positionFooter();

    function positionFooter() {
        var w = $(window).width();
        var header = $("#header");
        
        // Make main menu bar span whole width
        if(w <= 992)    header.css('width','100%');
        else            header.css('width','');
        
        if(w <= 768)    header.height(50);
        else            header.height('');


        footerHeight = $footer.height();
        var dheight = footerHeight + 70;
        //var wheight = $(window).height();
        var wheight = window.innerHeight;
        if (dheight < wheight) {
//            $footer.css({
//                position: "absolute"
//            }).animate({
//                bottom: "0"
//            })
        } else {
            $footer.css({
                position: "static"
            })
        }
    }

    $(window).resize(positionFooter);

    $(".navbar-toggle").click(function() {
        if (!$(this).hasClass("opened")) {
            footerHeight = $footer.height();
            var dheight = footerHeight + 305;
            var wheight = $(window).height();
            if (dheight < wheight) {
                $footer.css({
                    position: "absolute"
                }).animate({
                    bottom: "0"
                })
            } else {
                $footer.css({
                    position: "static"
                })
            }
            $(this).addClass("opened");

        } else {

            footerHeight = $footer.height();
            var dheight = footerHeight + 70;
            var wheight = $(window).height();
            if (dheight < wheight) {
                $footer.css({
                    position: "absolute"
                }).animate({
                    bottom: "0"
                })
            } else {
                $footer.css({
                    position: "static"
                })
            }
            $(this).removeClass("opened");
        }
    });
});