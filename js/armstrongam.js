var header 		= $("#header");
var container 	= $("#wrap-container");
var footer 		= $("#wrap-footer");
var sidebar 	= $("#wrap-sidebar");
var content 	= $("#wrap-content");
var cover 		= $("#content-cover");
var tooltips	= $(".tooltips");

$(window).load(windowload);
$(window).resize(windowload);
$(document).ready(init);

function init() 
{
	// set container height
	resizeContainer("onload");
}

function resizeContainer(bool) {
	var w = $(window).width();
	var h = $(window).height();
	var conta_height = h - header.height() - footer.height() - 30; // 30: margin

	if(bool === "onload") {
		if(container.height() < conta_height) 	
			container.height(conta_height);
		else container.height('');
	}
	else {
		container.height(conta_height);
	}
}

function windowload() 
{
	var w = $(window).width();

	// if(w < 992) {
	// 	// Fix About-Us tooltip
	// 	if(tooltips && typeof tooltips != undefined) {
	// 		tooltips.find('.img-tooltip').removeClass('right').removeClass('left');
	// 	}
	// }
	if(w < 768) {
		// resize when content is show
		if(content.css('display') == 'block') {
			sidebar.addClass('sidebar-hidden');
			container.addClass('container-show');
		}
	}
	else {
		resizeContainer();

		// reset content cover height
		if(cover.height() < container.height())
			cover.css('min-height', container.height());

		// resize when content is show
		if(sidebar.hasClass('sidebar-hidden')) {
			sidebar.removeClass('sidebar-hidden');
			container.removeClass('container-show');
		}

		// made fadeIn when is page loaded
		if(container.css("display") == "none")
			container.fadeIn(400);
	}
}


/*
 * 	MAIN EVENT
 */

sidebar.on('click', '.business', function()
{
	var w = $(window).width();
	var flag = $(this).attr('data-flag');
	var contentID = $(this).attr('data-id');

	if(contentID == null || typeof contentID == undefined)
		return false;

	if(w < 768) 
	{
		// slide sidebar + show content
		sidebar.addClass('sidebar-hidden');
		setTimeout(function()
		{
			// show item content
			container.addClass('container-show');
			content.addClass('content-show');

			if(flag === "contact")
				container.css('margin-bottom', 0);

			// reset content cover height
			if(cover.height() < sidebar.height())
				cover.css('min-height', sidebar.height());

			window.scrollTo(0, 1);
		}, 300);
	}

	// active item clicked
	sidebar.find('.business').addClass('faded');
	$(this).removeClass('faded');

	// hidden content cover ==> Tao hieu ung
	cover.fadeOut(300, function()
	{	
		// show item content
		content.find('.item-content').addClass('hidden');
		content.find(contentID).removeClass('hidden');

		$('#mCSB_2_dragger_vertical').css("top",0);
		$('#mCSB_2_container').css("top",0);

		// reset content cover height
		if(cover.height() < container.height())
			cover.css('min-height', container.height());
	});

	// show content cover
	cover.fadeIn('slow');
});

function content_close() 
{
	var w = $(window).width();
	cover.fadeOut('slow');

	if(w < 768)
	{
		// slide sidebar + show content
		sidebar.removeClass('sidebar-hidden');
		container.removeClass('container-show');
		content.removeClass('content-show');
	}
}