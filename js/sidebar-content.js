xs = 768;
mtop = 100;
mbottom = 10;

header = $("#header");
container = $("#wrap-container");
footer = $("#wrap-footer");
sidebar = $("#wrap-sidebar");
content = $("#wrap-content");
cover = $('#cover-container');

$(window).load(onWindowResize);
$(window).resize(onWindowResize);

$(document).ready(function()
{
    var w = $(window).width();
    
    if (w < xs)
    {
        cover.css('padding', '0px');
        container.css("margin-top", mtop+"px");
        container.css("margin-bottom", "0");
        footer.css("position","relative");
    }
    else
    {
        var h = $(window).height();

        cover.css('padding', '0px 15px');
        container.css("margin-top", "0");
        container.css("margin-bottom", mbottom+"px");
        sidebar.height(h - header.height() - footer.height() - mbottom);
        footer.css("position","absolute");
    }

    sidebar.find(".item").click( function()
    {
        sidebar.find('.item').addClass('faded');
        $(this).removeClass('faded');

        var id = $(this).attr("id");
        if (id != null && id != undefined)
            readmore(id);
    });

    //for mobile version
    if (w <= xs)
        return;

    //show the first item by default
    //note: the very first item may not be a valid content item
    var childItem = sidebar.find(".item").first().next();
    if(childItem) {
        var id = childItem.attr("id");
        if (id) childItem.trigger("click");
        else {
            var itemNews = childItem.find('.read-more');
            if (itemNews)  itemNews.trigger("click");
        }
    }
});

function readmore(id)
{
    var w = $(window).width();
    var h = $(window).height();
    var id = id.substr(5,99);

    if(content.css('display') == 'none')
        content.fadeIn('slow');

    // if (content.find("#content-close").length == 0)
    //     content.append('<span id="content-close" onclick="content_close()"></span>');

    if (w < xs)
    {
        header.height('50px');
        container.css('margin-top', 0);
        content.width(w-30).height('auto')
                .css('margin-bottom', mbottom+'px').css('margin-left', '0px');

        sidebar.animate({marginLeft:"-"+xs+"px"}, 500);
    }
    else
    {
        var w_content = cover.width() - sidebar.width() - 61;
        var r = h - header.height() - footer.height() - mbottom;

        content.width(w_content).height(r - 30)
                .css('overflow','auto').css('margin-left', '1px');
    }

    content.find(".item-content").hide();
    content.find("#item-content-"+id).fadeIn('slow');

    // setTimeout(function() {
    //     if (sidebar.height() < content.height())
    //     {
    //         sidebar.height(content.height());
    //         footer.css("position","relative");
    //     }
    //     else
    //     {
    //         content.height(sidebar.height());
    //     }
    // },250);   
}


function content_close()
{
    var w = $(window).width();
    if (w < xs) {
        content.fadeOut(100, function(){
            container.css('margin-top', mtop + 'px');
            header.height('100%');
        });
        sidebar.animate({marginLeft:0} ,250);
    }
    else {
        content.fadeOut(100);
    }
}


function onWindowResize()
{
    setTimeout(function() {
        var w = $(window).width();
        var c = false;
        var s = false;

        if (content.css('display') == 'block') c = true;
        if (parseInt(sidebar.css('margin-left')) == 0) s = true;

        if (w >= xs)
        {
            var h = $(window).height();
            var r = h - header.height() - footer.height() - mbottom;
            if (c == true)
            {
                // new: put cover container (350: sidebar width, 1: margin-left, 60: padding)
                var w_content = cover.width() - sidebar.width() - 61;
                content.width(w_content).height(r - 30).css('margin-left', '1px');

                if (s == false) sidebar.animate({marginLeft:0},500);
            }
            else
            {

            }

            sidebar.height(r);
            cover.css('padding', '0px 15px');
            container.css("margin-top", "0").css("margin-bottom", mbottom+"px");

            if (r > content.height())
                footer.css("position","absolute");
        }
        else
        {
            if (c == true)
            {
                container.css('margin-top', 0);
                content.width(w-30).height('auto')
                        .css('margin-left', 0)
                        .css('margin-bottom', 15);

                if (s == true) sidebar.animate({marginLeft:"-"+xs+"px"},500);
            }
            else
            {
                container.css("margin-bottom", 0);
                container.css("margin-top", mtop);
            }

            cover.css('padding', 0);
            sidebar.css("height","100%");
            footer.css("position","relative");
        }
    }, 100);   
};
