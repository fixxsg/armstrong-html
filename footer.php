        
        <div id="wrap-footer" class="footer-sticky">
            <div id="footer" class="container">
                <div class="row">
                    <div class="col-1 col-lg-3">
                        <a href="http://unpri.org/"><img class="signatory" src="images/signatory.png" alt="" /></a>
                        <img class="eunion" src="images/european-union.png" alt="" />
                    </div>
                    <div class="col-2 col-lg-6 col-md-8 col-lg-pull-1">
                        <p>Armstrong Asset Management is supported with financial assistance of the European Union.</p>
                        <p>The views expressed herein can in no way be taken to reflect the official opinion of the European Union.</p>
                    </div>
                    <div class="col-3 col-lg-3 col-md-4">
                        <p class="asm2014">© Armstrong Asset Management 2014</p>
                        <p class="ldc hidden-sm hidden-xs">
                            <a href="disclaimer.php">Legal Disclaimer</a>
                        </p>
                    </div>
                </div><!--end .row-->
            </div><!--end #footer-->
        </div><!--end #wrap-footer-->

        
        <!--insert jQuery-->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/jquery-1.11.1.min.js"><\/script>')</script>
        <script type="text/javascript" src="js/jquery.mCustomScrollbar.js"></script>
        <script src="js/bootstrap.js"></script>
        <!--End jQuery-->
        <script type="text/javascript">
            (function($){
                $(window).load(function(){
                    $("#wrap-sidebar").mCustomScrollbar({
                        theme:"minimal",
                        autoDraggerLength: false,
                        autoHideScrollbar: false,
                        mouseWheel:{ scrollAmount: 100 }
                    });

                    $('#wrap-content').mCustomScrollbar({
                        theme:"minimal",
                        autoDraggerLength: false,
                        autoHideScrollbar: false,
                        mouseWheel:{ scrollAmount: 100 }
                    });
                });
            })(jQuery);
        </script>
