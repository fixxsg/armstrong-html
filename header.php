<?php

$facebook_url = "http://www.facebook.com/ArmstrongAssetMgmt";
$linkedin_url = "http://www.linkedin.com/company/armstrong-asset-management-pte-ltd";

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />

        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">

        <title>Armstrong Asset Management</title>
        <meta name="description" content="" />
        <meta name="robots" content="index, follow" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="images/favicon.ico">

        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="css/scrollbar.css" />
        <link rel="stylesheet" type="text/css" href="style.css" />
        <link rel="stylesheet" type="text/css" href="css/investment-content-mb.css" />
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,700italic' rel='stylesheet' type='text/css' />

        <!-- tooltips css -->
        <link rel="stylesheet" type="text/css" href="css/tooltipster.css" />


        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="investement">
        <div id="header" class="container">
            <div class="row">
                <h1 id="logo" class="col-lg-2 col-md-3 hidden-sm hidden-xs"><a href="./">Armstrong Asset Management</a></h1>
                <div class="social col-lg-1 col-md-1 hidden-sm hidden-xs">
                    <a href="<?php echo $facebook_url; ?>" target="_blank" class="fb"><img src="images/fb-dark.png" alt="facebook" /></a>
                    <a href="<?php echo $linkedin_url; ?>" target="_blank" class="in"><img src="images/in-dark.png" alt="linkedin" /></a>
                </div>
                <div class="navbar navbar-default col-lg-9 col-md-8">
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li <?php if ($page == "aboutus") echo "class='active'"; ?>><a href="aboutus.php">About Us</a></li>
                            <li <?php if ($page == "investment") echo "class='active'"; ?>><a href="investment.php">Investment Portfolio</a></li>
                            <li <?php if ($page == "news") echo "class='active'"; ?>><a href="news.php">News</a></li>
                            <li <?php if ($page == "newsletter") echo "class='active'"; ?>><a href="energeyes_newsletter.php">EnergEyes Newsletter</a></li>
                            <li <?php if ($page == "contact") echo "class='active'"; ?>><a href="contact.php">Contact</a></li>
                            <li class="hidden-lg hidden-md nav_social">
                                <a href="#" class="nav_fb"><img src="images/fb-white.png" alt="facebook" /></a>
                                <a href="#" class="nav_in"><img src="images/in-white.png" alt="linkedin" /></a>
                            </li>
                        </ul>
                    </div>
                    <div class="navbar-header row">
                        <a class="navbar-brand hidden-lg hidden-md col-xs-9" href="./"><img src="images/logo-mobile.png" alt="Navigation"/></a>
                        <button type="button" class="navbar-toggle col-xs-2" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div><!--end .navbar -->
            </div><!--end .row -->
        </div><!--end #header-->

        