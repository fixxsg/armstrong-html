<?php
$page = "index";
    include ('header.php');
?>
        
        <div class="index-content" id="index-content"></div>
        <div class="footer-sticky index-sticky">
            <div id="wrap-hnews">
                <div class="hnews container">
                    <div class="intro">
                        <p><span>ARMSTRONG ASSET MANAGEMENT</span> is a clean energy fund manager committed to investments<br />
                        that will leave a long term positive impact on society and the natural environment..</p>
                    </div><!--end .intro-->

                    <div class="latest-news row">
                        <div class="col-lg-1 hidden-sm"><span class="tlnews">Lastest News</span></div>
                        <div class="post-news col-lg-11">
                            <div class="post clearfix">
                                <div class="row clearfix">
                                    <div class="cell1 col-md-1 col-sm-2 col-xs-3">2014.08.08</div>
                                    <div class="cell2 col-md-11 col-sm-10 col-xs-9"><a href="news.php?id=1" style="color:white; text-decoration:none;">Armstrong Teams Up With nv vogt to Replace Diesel Generation with Solar Power Plants in the Philippines</a></div>
                                </div>
                                <div class="row clearfix">
                                    <div class="cell1 col-md-1 col-sm-2 col-xs-3">2014.07.21</div>
                                    <div class="cell2 col-md-11 col-sm-10 col-xs-9"><a href="news.php?id=2" style="color:white; text-decoration:none;">Armstrong Enters Mini-Hydro Market in Indonesia with NKE Group</a></div>
                                </div>
                                <div class="row clearfix">
                                    <div class="cell1 col-md-1 col-sm-2 col-xs-3">2014.07.09</div>
                                    <div class="cell2 col-md-11 col-sm-10 col-xs-9"><a href="news.php?id=3" style="color:white; text-decoration:none;">The Blue Circle partners with Annex Power to Develop Wind Energy in Thailand</a></div>
                                </div>
                            </div>
                        </div><!--end .post-news-->
                    </div><!--end .latest-news-->
                </div><!--end #news-->
            </div><!--end #wrap-contain-->

            <div id="wrap-footer">
                <div id="footer" class="container">
                    <div class="row">
                        <div class="col-1 col-lg-3">
                            <a href="http://unpri.org/"><img class="signatory" src="images/signatory.png" alt="" /></a>
                            <img class="eunion" src="images/european-union.png" alt="" />
                        </div>
                        <div class="col-2 col-lg-6 col-md-8 col-lg-pull-1">
                            <p>Armstrong Asset Management is supported with financial assistance of the European Union.</p>
                            <p>The views expressed herein can in no way be taken to reflect the official opinion of the European Union.</p>
                        </div>
                        <div class="col-3 col-lg-3 col-md-4">
                            <p class="asm2014">© Armstrong Asset Management 2014</p>
                            <p class="ldc hidden-sm hidden-xs">
                                <a href="disclaimer.php">Legal Disclaimer</a>
                            </p>
                        </div>
                    </div><!--end .row-->
                </div><!--end #footer-->
            </div><!--end #wrap-footer-->
        </div>

        <!--insert jQuery-->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/jquery-1.11.1.min.js"><\/script>')</script>
        <script src="js/bootstrap.js"></script>
        <!--End jQuery-->

        <script>
            (function(){
                var header   = $('#header');
                var hnews    = $('#wrap-hnews');
                var footer   = $('#wrap-footer');
                var ClearFix = $('#index-content');
        
                $(document).ready(main);
                $(window).resize(main);

                function main() {
                    var h = $(window).height();
                    var w = $(window).width();
                    var cf_height = h - hnews.height() - header.height() - footer.height();

                    ClearFix.height(cf_height);
                }
            })(jQuery);
        </script>
    </body>
</html>