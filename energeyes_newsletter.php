<?php
	if (isset($_GET["id"])) $id = $_GET["id"];
    else                    $id = null;

	$page = "newsletter";
	include ('header.php');
?>
		<div id="wrap-container" class="container">
			<div id="wrap-sidebar" class=" col-xs-12 standard-wid">
          		<div id="sidebar">
	              	<div class="investment_portfolio item news">
	                  	<div class="stitle">Energeyes Newsletter</div>
	              	</div>

	            <?php 
	                if($id == null || $id == 1 || $id > 2)
	                    echo '<div class="business item news" data-id="#item-content-1">';
	                else 
	                    echo '<div class="business item news faded" data-id="#item-content-1">';
	            ?>
	                <p class="date">ENERGEYES AUGUST 2014</p>
	                <p class="title">
						If a picture is worth a thousand words....
	                </p>
	                <!-- <p class="describe">
	                    ....we want to put you fully in the picture where making the most of the energy from the sun is concerned. Not just because our latest commitment is to go for solar in a big way - $29 million investment in a comprehensive solar scheme in the Philippines.
	                </p> -->
	            </div>
			
				<div class="business item news <?php if($id != 2) echo 'faded'?>" data-id="#item-content-2">
	                <p class="date">EnergEyes - April 2014</p>
	                <p class="title">
	                	The renewable energy pipeline....
	                </p>
	                <!-- <p class="describe">
						It might be news to you that there's a pipeline of about 15 deals at various stages of evaluation and negotiation for the Armstrong South East Asia Clean Energy Fund, with a corresponding funding requirement of over US$1 billion. More quality deals are expected to be originated through our new strategic partnership with Mandiri Investment Management (see first article below).
	                </p> -->
              	</div>
          </div>
      	</div>
			<div id="wrap-content">
				<div id="content-cover" class="col-md-12 content-cover">
					<?php 
                        if($id == null || $id == 1 || $id > 2)
                            echo '<div class="item-content" id="item-content-1">';
                        else 
                            echo '<div class="item-content hidden" id="item-content-1">';
                    ?>
						<h2 class="contentheading"></h2>
						<div class="article-content clearfix">
						    <ul id="background-table" class="col-lg-12 col-md-12 col-sm-12" cellpadding="0" cellspacing="0">
							    <li style="width:100%">
						            <table style="-premailer-cellpadding: 0; -premailer-cellspacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%" cellpadding="0" cellspacing="0">
						                <tr style="background: #111; color: #fff; font-size: 24px; font-weight: bold; height: 48px; letter-spacing: -0.25px; line-height: 48px; text-transform: uppercase; width: 100%" bgcolor="#111">
						                    <td style="border-collapse: collapse;">
						                        <img src="http://assets.fixx.sg/clients/armstrong/edm/logo.png" style="-ms-interpolation-mode: bicubic; display: block; outline: none; text-decoration: none" />
						                    </td>
						                    <td class="header-title" align="right" style="border-collapse: collapse; padding-left: 30px;">Energeyes August 2014</td>
						                </tr>
						            </table>
							    </li>
							    <li style="width:100%">
							        <img width="100%" src="http://assets.fixx.sg/clients/armstrong/edm/hero.jpg"/>
							    </li>
							    <li class="item-header">
							    	<p class="last-title">If a picture is worth a thousand words....</p>
							    	<div class="header-left">
							    		<img src="http://ih.constantcontact.com/fs146/1102918945378/img/1929.jpg"/>		
							    	</div>
							    	<div class="header-right">
							    		<p style="color: #aaa; font-size: 0.875em; line-height: 20px; margin: 0; padding-bottom: 20px">....we want to put you fully in the picture where making the most of the energy  from the sun is concerned. Not just because our latest commitment is to go for solar in a big way - $29 million investment in a comprehensive solar scheme in the Philippines. But elsewhere in the region and the world we are seeing that solar energy is taking off - on the ground, on the roof, for charging electric cars, for shopping centres, to make buildings - even airports - more efficient. So much so that power companies in American are starting to worry that they are losing out. We can only say 'if you can't beat them, join them'. Make the switch to solar - and other suitable renewables, of course - to get off our fossil fuel dependency and cut our emissions to the bone. Zero carbon electricity is within our reach, says Jeffrey Sachs, so what's stopping us? Seeing is believing, after all.<br />
				                            <span style="color: #fff">- Andrew Affleck</span>
				                        </p>
							    	</div>
							    </li>
							    <li class="item-inline">
							        South East Asia Renewable News
							    </li>
							    <li class="li-item">
									<div class="item-left">
										<img src="http://ih.constantcontact.com/fs146/1102918945378/img/1916.jpg" />
									</div>
									<div class="item-right">
										<p class="title" >Partnering with nv vogt to replace diesel with solar</p>
				                        <p class="content">Armstrong Asset Management has committed to provide US$29 million to fund the construction of a pipeline of bilateral solar power projects in the Philippines being developed by the development company, nv vogt Singapore, getting underway with a 6.25 MW ground-mounted and grid-connected solar power plant in South Cotabato in the southern region of Mindanao.</p>
				                        <a class="read" href="/news.php?id=1">
				                        	Read More
				                        </a>
									</div>
							    </li>
								<li class="li-item">
									<div class="item-left">
										<img src="http://ih.constantcontact.com/fs146/1102918945378/img/1917.jpg" />
									</div>
									<div class="item-right">
										<p class="title" >Ikea feeds clean energy to Thai grid with new solar roof</p>
				                        <p class="content">Singapore-based firms Ikano and Phoenix Solar have teamed up to install an array of 4,000 solar panels atop Bangkok's Megabangna shopping mall to supply renewable energy to the city's grid.Ikea becomes one of the first companies in Thailand to feed renewable energy into the city grid under a feed-in tariff programme launched by the government last year.</p>
				                        <a class="read" href="/news.php?id=2">
				                        	Read More
				                        </a>
									</div>
							    </li>
								<li class="li-item">
									<div class="item-left">
										<img src="http://ih.constantcontact.com/fs146/1102918945378/img/1918.jpg" />
									</div>
									<div class="item-right">
										<p class="title" >Roadmaps address energy & climate change challenges</p>
				                        <p class="content">The Singapore Government will pump in S$100 million to fund two initiatives in energy research and development (R&D), specifically in building energy efficiency and research on green data centres. This was announced at the first Energy Technology Roadmap Symposium, where five road maps were revealed, covering solar panel research, carbon capture and storage or utilisation, green data centres, building energy efficiency and industry energy efficiency.</p>
				                        <a class="read" href="/news.php?id=3">
				                        	Read More
				                        </a>
									</div>
							    </li>
							    <li class="li-item">
									<div class="item-left">
										<img src="http://ih.constantcontact.com/fs146/1102918945378/img/1919.jpg" />
									</div>
									<div class="item-right">
										<p class="title" >Partnering for Transactions in Indonesia & Asia</p>
				                        <p class="content">The Asian Development Bank (ADB) will provide a loan of up to $50 million, supported by the Clean Technology Fund, to share risks with the private sector during the project development and exploration phase of the Rantau Dedap geothermal project in Indonesia. The ADB also announced it is forming Asia Climate Partners (ACP) with ORIX Corporation (ORIX) and Robeco Institutional Asset Management B.V. (Robeco)  to undertake commercially-oriented private equity investments across a variety of environmentally supportive, low-carbon transactions throughout Asia.</p>
				                        <a class="read" href="/news.php?id=4">
				                        	Read More
				                        </a>
									</div>
							    </li>
							    <li class="item-inline">
						        	Global Renewable Energy News
						    	</li>
							    <li class="li-item">
									<div class="item-left">
										<img src="http://ih.constantcontact.com/fs146/1102918945378/img/1920.png" />
									</div>
									<div class="item-right">
										<p class="title" >Counter proposal to end US-China trade dispute?</p>
				                        <p class="content">TThe US Solar Energy Industries Association (SEIA) has urged SolarWorld Americas LLC  to offer a specific proposal in renewed attempts to end the US - China solar trade dispute. As SEIA represents the entire solar value chain in the United States - including manufacturers, installers, financiers, engineers, project developers, consultants and retailers - it is committed to finding an "industry-wide solution."</p>
				                        <a class="read" href="/news.php?id=4">
				                        	Read More
				                        </a>
									</div>
							    </li>
								<li class="li-item">
									<div class="item-left">
										<img src="http://ih.constantcontact.com/fs146/1102918945378/img/1921.jpg" />
									</div>
									<div class="item-right">
										<p class="title" >First hybrid solar-wind renewable energy tower</p>
				                        <p class="content">The small Arizona border town of San Luis will be the site of the first Solar Wind Energy Tower installation in the world. The US$1.5 billion project would generate electricity through the use of ambient desert heat that passes through a concrete structure its proponents say would be the tallest in North America. When completed it will generate 425 megawatts of electricity each year.  A typical home in Arizona consumes approximately 12 megawatt hours of electricity annually.</p>
				                        <a class="read" href="/news.php?id=4">
				                        	Read More
				                        </a>
									</div>
							    </li>
								<li class="li-item">
									<div class="item-left">
										<img src="http://ih.constantcontact.com/fs146/1102918945378/img/1922.jpg" />
									</div>
									<div class="item-right">
										<p class="title" >Which electric car killed off the power companies?</p>
				                        <p class="content">Morgan Stanley thinks Tesla Motors is going to help kill power companies in the US. That's because the electric car business was pushing to develop better energy storage technology and then mass manufacture batteries. That's exactly what Tesla CEO Elon Musk - also the chairman of Solar City - and his company will be doing at its forthcoming Gigafactory, which it is building in the US Southwest with Panasonic.</p>
				                        <a class="read" href="/news.php?id=4">
				                        	Read More
				                        </a>
									</div>
							    </li>
								<li class="li-item">
									<div class="item-left">
										<img src="http://ih.constantcontact.com/fs146/1102918945378/img/1923.jpg" />
									</div>
									<div class="item-right">
										<p class="title" >Energy efficient ice cream freezers take more cars off the road</p>
				                        <p class="content">New, hyper-efficient freezers, utilising improved insulation, high-efficiency compressors and LED lighting have the potential to achieve an industry-leading 70% energy reduction, resulting in CO2 savings equivalent to removing half a million cars from the road.The refrigeration innovation falls under Unilever's Sustainable Living Plan to double the size of its business while reducing its environmental impact.</p>
				                        <a class="read" href="/news.php?id=4">
				                        	Read More
				                        </a>
									</div>
							    </li>
							    <li class="li-item">
									<div class="item-left">
										<img src="http://ih.constantcontact.com/fs146/1102918945378/img/1924.jpg" />
									</div>
									<div class="item-right">
										<p class="title" >UK scientists develop spray-on solar energy cells</p>
				                        <p class="content">Perovskite solar cells, considered one of the major scientific breakthroughs of recent years, could be made available in a spray can after the product was developed by scientists at the University of Sheffield in the United Kingdom. This means that solar cells could be applied to almost anything; an electric car could generate energy from its coat of perovskite solar paint.</p>
				                        <a class="read" href="/news.php?id=4">
				                        	Read More
				                        </a>
									</div>
							    </li>
							    <li class="li-item">
									<div class="item-left">
										<img src="http://ih.constantcontact.com/fs146/1102918945378/img/1925.jpg" />
									</div>
									<div class="item-right">
										<p class="title" >Is politics the biggest stumbling block to clean energy?</p>
				                        <p class="content">A comprehensive new Guide to Sustainable Clean Energy expertly sets out the necessity, benefits and opportunities involved in making the switch. It delves into the surprisingly distant history of renewable energy, dissects the biggest challenges facing the sector today, and asks what might be powering our hoverbikes in the faraway future. Along the way, you'll meet some of the leaders, going to great lengths to bring about the low-carbon transition.</p>
				                        <a class="read" href="/news.php?id=4">
				                        	Read More
				                        </a>
									</div>
							    </li>						
								<li class="li-item">
									<div class="item-left">
										<img src="http://ih.constantcontact.com/fs146/1102918945378/img/1928.jpg" />
									</div>
									<div class="item-right">
										<p class="title" >Rome & Milan Airports get energy efficiency test underway</p>
				                        <p class="content">A European Union study shows that 500 airports in the 28 member countries together emit as much CO2 as a city of 50 million people. Airport buildings are disastrously inefficient structures which produce large quantities of greenhouse gases. With new airports and vast terminals being built across the planet, pressure is on the airline industry to improve its performance, so in a bid to try to curb the problem, the EU has begun a three year programme to cut airport emissions by 20%.</p>
				                        <a class="read" href="/news.php?id=4">
				                        	Read More
				                        </a>
									</div>
							    </li>
							    <li class="item-inline">
						        	 Events and Appointments of Note
						    	</li>
							    <li style="width:100%;">
							        <img src="http://ih.constantcontact.com/fs146/1102918945378/img/1926.png" style="width: 100%;" />
							    </li>
						    	<li class="li-item">
						    		<div style="width:100%;">
										<p class="title" >How buildings can generate energy & save it at the same time</p>
				                        <p class="content">From 1 - 3 September 2014, the Singapore Green Building Week (SGBW) will play host to international green building experts, policy-makers, academics and built environment practitioners, for a congregation of ideas, collaboration and learning, to achieve a shared vision of a greener planet through the green-building movement.</p>
				                        <a class="read" href="/news.php?id=4">
				                        	Read More
				                        </a>
			                        </div>
							    </li>
							    <li class="item-inline">
							        Commentary
							    </li>
							    <li class="li-last-item">
							    	<div class="item-left">
				                        <p class="title">
				                            Is politics the biggest stumbling block to clean energy?
				                        </p>
				                        <p class="content">
				                            A comprehensive new Guide to Sustainable Clean Energy expertly sets out the necessity, benefits and opportunities involved in making the switch. It delves into the surprisingly distant history of renewable energy, dissects the biggest challenges facing the sector today, and asks what might be powering our hoverbikes in the faraway future. Along the way, you'll meet some of the leaders, going to great lengths to bring about the low-carbon transition.
				                        </p>
				                        <a class="read">Read More</a>
							    	</div>
							    	<div class="item-right">
							    		<img src="http://ih.constantcontact.com/fs146/1102918945378/img/1927.jpg" />
							    	</div>
							    </li>
							    <li class="li-footer-item">
						            <table style="-premailer-cellpadding: 0; -premailer-cellspacing: 0; background: #000; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%" bgcolor="#000" cellpadding="0" cellspacing="0">
						                <tr>
						                    <td style="border-collapse: collapse; padding: 20px 30px">
						                        <p style="color: #ccc; font-size: 12px; line-height: 20px; margin: 0; padding-bottom: 3px; text-transform: uppercase">Armstrong asset management</p>
						                        <p style="color: #777; font-size: 12px; line-height: 16px; margin: 0; padding-bottom: 3px">30 Raffles Place #10-04 Chevron House Singapore 048622<br />
						                            +65 6922 9790
						                        </p>
						                        <a style="color: #3d9733; cursor: pointer; font-size: 12px; text-decoration: none">www.armstrongam.com</a>
						                    </td>
						                    <td class="footer-secon-row" style="border-collapse: collapse; padding-right: 30px">
						                        <table style="-premailer-cellpadding: 0; -premailer-cellspacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%" cellpadding="0" cellspacing="0">
						                            <tr style="height: 37px">
						                                <td valign="top" align="right" style="border-collapse: collapse">
						                                    <a href="https://www.facebook.com/ArmstrongAssetMgmt" style="color: #2892eb; cursor: pointer; text-decoration: none"><img src="http://assets.fixx.sg/clients/armstrong/edm/fb.png" style="-ms-interpolation-mode: bicubic; border: none; display: block; outline: none; text-decoration: none" /></a>
						                                </td>
						                                <td valign="top" align="right" style="border-collapse: collapse">
						                                    <a href="https://www.linkedin.com/company/armstrong-asset-management-pte-ltd" style="color: #2892eb; cursor: pointer; text-decoration: none"><img src="http://assets.fixx.sg/clients/armstrong/edm/linkedin.png" style="-ms-interpolation-mode: bicubic; border: none; display: block; outline: none; text-decoration: none" /></a>
						                                </td>
						                            </tr>
						                            <tr style="height: 24px">
						                                <td style="border-collapse: collapse">
						                                    <a href="http://www.unpri.org/" style="color: #2892eb; cursor: pointer; text-decoration: none"><img src="http://assets.fixx.sg/clients/armstrong/edm/pri.png" style="-ms-interpolation-mode: bicubic; border: none; display: block; outline: none; text-decoration: none" /></a>
						                                </td>
						                                <td align="right" style="border-collapse: collapse">
						                                    <a style="color: #2892eb; cursor: pointer; text-decoration: none"><img src="http://assets.fixx.sg/clients/armstrong/edm/flag.png" style="-ms-interpolation-mode: bicubic; border: none; display: block; outline: none; text-decoration: none" /></a>
						                                </td>
						                            </tr>
						                        </table>
						                    </td>
						                </tr>
						            </table>
							    </li>
							</ul>
						</div>
					</div>

					<div class="item-content <?php if($id != 2) echo 'hidden'?>" id="item-content-2">
						<h2 class="contentheading"><span>EnergEyes - April 2014</span></h2>
						<div class="article-content clearfix" style="padding: 10px 10px 30px 10px; background-color:#FFFFFF;">
						    <div style="width:100%;background-color:#008000;padding:1px 1px 1px 1px;">
	                            <table style="background-color:#ffffff;" mce_style="background-color:#ffffff;" bgcolor="#ffffff" border="0" width="100%" cellspacing="0" cellpadding="0" class="mceItemTable">
                                    <tbody>
                                        <tr>
                                            <td valign="top" rowspan="1" colspan="1" align="center">
                                                <table style="background:transparent;" mce_style="background:transparent;" border="0" width="100%" cellspacing="0" cellpadding="0" class="mceItemTable">
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding:0px 0px 0px 0px;" mce_style="padding:0px 0px 0px 0px;" valign="top" width="100%" rowspan="1" colspan="1" align="center">
                                                                <table style="display: table;" mce_style="display: table;" border="0" width="100%" cellspacing="0" cellpadding="0" id="content_LETTER.BLOCK3" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="padding:0px 0px 0px 0px;color: rgb(0, 0, 0);" mce_style="padding:0px 0px 0px 0px;color: #000000;" valign="top" rowspan="1" colspan="1" align="center">
                                                                                <br/>
                                                                                <table class="imgCaptionTable mceItemTable" style="text-align: center; margin-top: 5px; margin-bottom: 5px;" mce_style="text-align: center; margin-top: 5px; margin-bottom: 5px;" width="100%">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="imgCaptionImg" style="text-align: center; color: #000000;" mce_style="text-align: center; color: #000000;" width="100%" rowspan="1" colspan="1"><img height="222" vspace="0" border="0" name="ACCOUNT.IMAGE.1846" hspace="0" width="100%" src="http://ih.constantcontact.com/fs146/1102918945378/img/1846.jpg" mce_src="http://ih.constantcontact.com/fs146/1102918945378/img/1846.jpg"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="imgCaptionText" style="text-align: center; font-style: normal; font-weight: normal; color: #1d1662; font-family: Tahoma, Arial, sans-serif; font-size: 14pt;" mce_style="text-align: center; font-style: normal; font-weight: normal; color: #1d1662; font-family: Tahoma, Arial, sans-serif; font-size: 14pt;" rowspan="1" colspan="1">
                                                                                                <div style="text-align: left;" mce_style="text-align: left;" align="left"><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; April 2014</strong></div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <div style="text-align: center;" mce_style="text-align: center;" align="center"><br/></div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <table style="background-color: #000036;" mce_style="background-color: #000036;" bgcolor="#000036" border="0" width="100%" cellpadding="0" cellspacing="0" id="content_LETTER.BLOCK4" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="color:#FFFFFF;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:5px 8px 5px 8px;" mce_style="color:#FFFFFF;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:5px 8px 5px 8px;" valign="top" rowspan="1" colspan="1" align="center"></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table style="background:transparent;" mce_style="background:transparent;" border="0" width="100%" cellspacing="0" cellpadding="0" class="mceItemTable">
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding:0px 12px 0px 12px;" mce_style="padding:0px 12px 0px 12px;" valign="top" width="100%" rowspan="1" colspan="1" align="center">
                                                                <table border="0" width="100%" cellspacing="0" cellpadding="0" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="padding-bottom:11px;height:1px;line-height:1px;" height="1" rowspan="1" colspan="1" align="center"><img height="1" vspace="0" border="0" hspace="0" style="display: block;" mce_style="display: block;" alt="" src="http://img.constantcontact.com/letters/images/1101116784221/S.gif" mce_src="http://img.constantcontact.com/letters/images/1101116784221/S.gif"></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table style="background:transparent;" mce_style="background:transparent;" border="0" width="100%" cellspacing="0" cellpadding="0" class="mceItemTable">
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding:0px 12px 0px 12px;" mce_style="padding:0px 12px 0px 12px;" valign="top" width="100%" rowspan="1" colspan="1" align="center">
                                                                <table border="0" width="100%" cellspacing="0" cellpadding="0" id="content_LETTER.BLOCK15" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="padding:7px 8px 7px 8px;font-size: 11pt; font-family: Georgia, 'Times New Roman', Times, serif; color: rgb(0, 0, 0);" mce_style="padding:7px 8px 7px 8px;font-size: 11pt; font-family: Georgia, 'Times New Roman', Times, serif; color: #000000;" valign="top" rowspan="1" colspan="1" align="left">
                                                                                <div style="text-align: justify; font-family: Tahoma, Arial, sans-serif;" mce_style="text-align: justify; font-family: Tahoma, Arial, sans-serif;" align="justify">
                                                                                    <div style="font-size: 16pt; color: #000036;" mce_style="font-size: 16pt; color: #000036;"><strong>The Renewable Energy Pipeline</strong></div>
                                                                                    <div>
                                                                                        <div>It might be news to you that there's a pipeline&nbsp;of&nbsp;about 15&nbsp;deals at various stages of evaluation and negotiation for the Armstrong South East Asia Clean Energy Fund, with a corresponding funding requirement of over US$1 billion.&nbsp;</div>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="font-size: 11pt; color: #000000; text-align: justify;" mce_style="font-size: 11pt; color: #000000; text-align: justify;" align="justify">
                                                                                    <div style="font-family: Tahoma, Arial, sans-serif;" mce_style="font-family: Tahoma, Arial, sans-serif;">
                                                                                        <div>More quality deals are expected to be originated through our new strategic partnership with <img height="168" vspace="5" name="ACCOUNT.IMAGE.1828" border="0" hspace="5" width="148" src="http://ih.constantcontact.com/fs146/1102918945378/img/1828.jpg" mce_src="http://ih.constantcontact.com/fs146/1102918945378/img/1828.jpg" align="right"> Mandiri Investment Management (see first article below).</div>
                                                                                        These are &nbsp;examples of the sort of information you might miss if we don't communicate directly and regularly with you, our project partners, investors and, in fact, all stakeholders and &nbsp;those interested in the funding and development of renewable energy capacity in &nbsp;the region. &nbsp;
                                                                                    </div>
                                                                                    <div style="font-family: Tahoma, Arial, sans-serif;" mce_style="font-family: Tahoma, Arial, sans-serif;">Just like we set out two and a half years ago to start this unique fund - which had its final close at $164 million last November - &nbsp;we are now embarking on a communications process, not just to keep you up to date on what we're doing, but to make sure you don't miss out on items of interest from the world of renewable energy. &nbsp;</div>
                                                                                    <div style="font-family: Tahoma, Arial, sans-serif;" mce_style="font-family: Tahoma, Arial, sans-serif;">We welcome your comments, input, ideas and news. You can also find us on Facebook and LinkedIn.&nbsp;</div>
                                                                                    <div style="font-family: Tahoma, Arial, sans-serif;" mce_style="font-family: Tahoma, Arial, sans-serif;">All ways to enable you to share in what we are doing and what we care about. We'll keep our eyes out for what's going on and we'll continue to energise South East Asia in a clean, responsible fashion.</div>
                                                                                    <div>
                                                                                        <div style="font-family: Tahoma, Arial, sans-serif;" mce_style="font-family: Tahoma, Arial, sans-serif;">Here's to a clean energy, low carbon future! &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
                                                                                        <div><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</strong></div>
                                                                                        <div><strong>Andrew Affleck</strong></div>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <a name="LETTER.BLOCK47" class="mceItemAnchor"></a>
                                                                <table style="background-color: #000036;" mce_style="background-color: #000036;" bgcolor="#000036" border="0" width="100%" cellspacing="0" cellpadding="0" id="content_LETTER.BLOCK47" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="padding:5px 8px 5px 8px;font-size: 12pt; font-family: Tahoma, Arial, sans-serif; color: rgb(255, 255, 255);" mce_style="padding:5px 8px 5px 8px;font-size: 12pt; font-family: Tahoma, Arial, sans-serif; color: #ffffff;" valign="top" rowspan="1" colspan="1" align="left">
                                                                                <div><b>South East Asia Renewable News&nbsp;</b></div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <a name="LETTER.BLOCK17" class="mceItemAnchor"></a>
                                                                <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content_LETTER.BLOCK17" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" valign="top" rowspan="1" colspan="1" align="left">
                                                                                <div>
                                                                                    <div style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;">
                                                                                        <strong style="color: #333333; line-height: 115%;" mce_style="color: #333333; line-height: 115%;">
                                                                                            <img height="153" vspace="5" name="ACCOUNT.IMAGE.1836" border="0" hspace="5" width="100%" src="http://ih.constantcontact.com/fs146/1102918945378/img/1836.jpg" mce_src="http://ih.constantcontact.com/fs146/1102918945378/img/1836.jpg" align="left">
                                                                                            <div style="font-family: Tahoma, Arial, sans-serif; color: #000036; font-size: 12pt;" mce_style="font-family: Tahoma, Arial, sans-serif; color: #000036; font-size: 12pt;">Co-investment in Indonesia's emerging hydro power sector &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
                                                                                        </strong>
                                                                                        <div style="text-align: justify; font-family: Tahoma, Arial, sans-serif;" mce_style="text-align: justify; font-family: Tahoma, Arial, sans-serif;" align="justify"><span style="line-height: 115%;" mce_style="line-height: 115%;">Mandiri Inves</span><span style="font-size: 11pt; line-height: 115%;" mce_style="font-size: 11pt; line-height: 115%;">tment Management&nbsp;has entered into a co-investment arrangement with Armstrong Asset Management to work together to invest in renewable energy projects in Indonesia. &nbsp; </span><span style="font-size: 11pt; line-height: 115%;" mce_style="font-size: 11pt; line-height: 115%;">Both companies will start their collaboration by focusing on the emerging hydro sector in Indonesia, in particular mini hydro-electric power plants. <a style="color: blue; text-decoration: underline;" mce_style="color: blue; text-decoration: underline;" track="on" shape="rect" mce_shape="rect" href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3Q-LrUlH-ANkzcPxTcWsgoNZ7WH5wTHu3LEOTJ8ajTpzX-ixGpONdxmGgw8rOGwrx3gnr3XZZPf0OxhVO0B9NjAc8g9Lz48kycolrdfXxo1Ylev-TOxV-SobyamgTc3Ddumt6nyKbGGYOt52Q9fyc-TrbdglxD89R7pX4fxrTpJ9JdotkVSJniltIJqekY_cgreUAdla0y8f_TZLLNd5uwqhkkD0cCos3VV6-yzevEK56RfmXvSx7uwGfbclXEL3bmHX9ayAkN17LsVL02ua9t3vVw9UACiVFBxNvNjirmFq2pJlItNRaAvevgvmV1wwZv-PM3d-0-jhEp356IKhDfNR4hKKMDBjWSDPYUp29Imp5WW5c99_XHr6Qhpk-G6CRdMEvXvFXa5tjGQy5mOwPMVRZLGf5xNIRuThYp556-N9g==" mce_href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3Q-LrUlH-ANkzcPxTcWsgoNZ7WH5wTHu3LEOTJ8ajTpzX-ixGpONdxmGgw8rOGwrx3gnr3XZZPf0OxhVO0B9NjAc8g9Lz48kycolrdfXxo1Ylev-TOxV-SobyamgTc3Ddumt6nyKbGGYOt52Q9fyc-TrbdglxD89R7pX4fxrTpJ9JdotkVSJniltIJqekY_cgreUAdla0y8f_TZLLNd5uwqhkkD0cCos3VV6-yzevEK56RfmXvSx7uwGfbclXEL3bmHX9ayAkN17LsVL02ua9t3vVw9UACiVFBxNvNjirmFq2pJlItNRaAvevgvmV1wwZv-PM3d-0-jhEp356IKhDfNR4hKKMDBjWSDPYUp29Imp5WW5c99_XHr6Qhpk-G6CRdMEvXvFXa5tjGQy5mOwPMVRZLGf5xNIRuThYp556-N9g==" linktype="1" target="_blank">Read More</a></span></div>
                                                                                        <div style="font-family: Tahoma, Arial, sans-serif; text-align: justify;" mce_style="font-family: Tahoma, Arial, sans-serif; text-align: justify;" align="justify"><br/></div>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <a name="LETTER.BLOCK37" class="mceItemAnchor"></a>
                                                                <table border="0" width="100%" cellspacing="0" cellpadding="0" id="content_LETTER.BLOCK37" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="padding:7px 8px 7px 8px;color: rgb(0, 0, 0); font-family: Georgia, 'Times New Roman', Times, serif;" mce_style="padding:7px 8px 7px 8px;color: #000000; font-family: Georgia, 'Times New Roman', Times, serif;" valign="top" rowspan="1" colspan="1" align="left">
                                                                                <div>
                                                                                    <div>
                                                                                        <div style="font-size: 12pt;" mce_style="font-size: 12pt;">
                                                                                            <strong>
                                                                                                <img height="121" vspace="5" border="0" name="ACCOUNT.IMAGE.1832" hspace="5" width="100%" src="http://ih.constantcontact.com/fs146/1102918945378/img/1832.jpg" mce_src="http://ih.constantcontact.com/fs146/1102918945378/img/1832.jpg" align="left">
                                                                                                <div style="color: #000027; font-family: Tahoma, Arial, sans-serif;" mce_style="color: #000027; font-family: Tahoma, Arial, sans-serif;">Presidential seal for solar plant project</div>
                                                                                            </strong>
                                                                                        </div>
                                                                                        <div style="font-size: 11pt; text-align: justify; font-family: Tahoma, Arial, sans-serif;" mce_style="font-size: 11pt; text-align: justify; font-family: Tahoma, Arial, sans-serif;" align="justify">The 50 MW solar power plant being initiated by&nbsp;<a shape="rect" mce_shape="rect">1Malaysia Development Bhd&nbsp;</a>(1MDB) has secured a tariff rate of somewhere between 40sen to 46sen per&nbsp;kWh, lower than existing tariff for electricity generated by solar plants, according to a online report in The Star. At the same time it was announced that&nbsp;the relevant parties were working to put this agreement together in time for it to be announced during the visit by United States President&nbsp;<a shape="rect" mce_shape="rect">Barack Obama</a>&nbsp;to Malaysia this month (April). <a style="color: blue; text-decoration: underline;" mce_style="color: blue; text-decoration: underline;" track="on" shape="rect" mce_shape="rect" href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3QNnmWCYdm9o-g9F-gkVr_U7JMQKWuNUA8oIYF5WjtltDGxdho60nANnEh8fDD1lcxLrXngleCw9fGFdmcbnJ0oMjEDdIMG9J0C3GM57f4utbWyM1s8j87MPMQI7FTwINHA0R5Kmicaj9Gb0h5SJtMy2sd0Mswc0MEYZeS98yYNrGK3a7BPLRQ-qhTUvmttM5gZjZ-kMfOwWVnUj3xwiLaXN94Ym2C_DkKrPXpd4CWmQVZ18fmTMC2nDBLW1CrIyj5lSm1EY3tdXXAKtrIjnyyF2iXwlaPJuz8L9feZdTPmeTXfQKx-WasqYmk9A8twEA7JCteOWesCJw3o7sL0_R9kVYwljC5Qpu0=" mce_href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3QNnmWCYdm9o-g9F-gkVr_U7JMQKWuNUA8oIYF5WjtltDGxdho60nANnEh8fDD1lcxLrXngleCw9fGFdmcbnJ0oMjEDdIMG9J0C3GM57f4utbWyM1s8j87MPMQI7FTwINHA0R5Kmicaj9Gb0h5SJtMy2sd0Mswc0MEYZeS98yYNrGK3a7BPLRQ-qhTUvmttM5gZjZ-kMfOwWVnUj3xwiLaXN94Ym2C_DkKrPXpd4CWmQVZ18fmTMC2nDBLW1CrIyj5lSm1EY3tdXXAKtrIjnyyF2iXwlaPJuz8L9feZdTPmeTXfQKx-WasqYmk9A8twEA7JCteOWesCJw3o7sL0_R9kVYwljC5Qpu0=" linktype="1" target="_blank">Read More</a></div>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <a name="LETTER.BLOCK38" class="mceItemAnchor"></a>
                                                                <table border="0" width="100%" cellspacing="0" cellpadding="0" id="content_LETTER.BLOCK38" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" valign="top" rowspan="1" colspan="1" align="left">
                                                                                <img height="119" vspace="5" border="0" name="ACCOUNT.IMAGE.1842" hspace="5" width="100" src="http://ih.constantcontact.com/fs146/1102918945378/img/1842.jpg" mce_src="http://ih.constantcontact.com/fs146/1102918945378/img/1842.jpg" align="left">
                                                                                <div style="font-family: Tahoma, Arial, sans-serif; font-size: 11pt;" mce_style="font-family: Tahoma, Arial, sans-serif; font-size: 11pt;">
                                                                                    <div style="color: #000000;" mce_style="color: #000000;">
                                                                                        <p style="margin-top: 0px; margin-bottom: 0px; font-size: 12pt; font-family: Tahoma, Arial, sans-serif; text-align: justify; color: #000027;" mce_style="margin-top: 0px; margin-bottom: 0px; font-size: 12pt; font-family: Tahoma, Arial, sans-serif; text-align: justify; color: #000027;" align="justify"><strong>Philippines gains new lending portfolio</strong></p>
                                                                                        <p style="margin-top: 0px; margin-bottom: 0px; font-size: 11pt; font-family: Tahoma, Arial, sans-serif; text-align: justify;" mce_style="margin-top: 0px; margin-bottom: 0px; font-size: 11pt; font-family: Tahoma, Arial, sans-serif; text-align: justify;" align="justify">The International Finance Corporation (IFC) of the World Bank Group and Europe's Thomas Lloyd Group Ltd inked a mandate-letter for the provision of US$330-million lending portfolio for renewable energy (RE) projects in the Philippines, the Manilla Bulletin reported 5 March. &nbsp;It noted that the facility "will augment the $87 million of development and construction capital already deployed or committed by the Thomas Lloyd Group of Companies and the Thomas Lloyd Cleantech Infrastructure Fund." &nbsp;<a style="color: blue; text-decoration: underline;" mce_style="color: blue; text-decoration: underline;" track="on" shape="rect" mce_shape="rect" href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3RC5HrjfTeuVr_eeD-Vil4srPFQAXhSf6pARgYqDAZi57G7G0tzEKTgzGflzLyVTE0mWrtS96vl6-wEp5Q7EmkuqePqHdyYkJQSF-qwbZBNmGZNLRq7DcIXLJotxzsQZVoH91mmf5Wi0PKJWzTjmrTFp47xk-QcIywlW1QIv99xoZg4HQEzLNG9SZG4NfGk2FFtyBoLilni90jwp9j8lQnnGkqBhVc6F-ffE_jogtHj2bZA8F-fqqg61ZBpLlWGrhufOQlRLLAOuDIObOeJYLLxns15ldj2fsVRC8e8BbYXsJ6i3cUCvYBXIBQnea0K9qSEpkxvBWHcHb7kR-GysSMV" mce_href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3RC5HrjfTeuVr_eeD-Vil4srPFQAXhSf6pARgYqDAZi57G7G0tzEKTgzGflzLyVTE0mWrtS96vl6-wEp5Q7EmkuqePqHdyYkJQSF-qwbZBNmGZNLRq7DcIXLJotxzsQZVoH91mmf5Wi0PKJWzTjmrTFp47xk-QcIywlW1QIv99xoZg4HQEzLNG9SZG4NfGk2FFtyBoLilni90jwp9j8lQnnGkqBhVc6F-ffE_jogtHj2bZA8F-fqqg61ZBpLlWGrhufOQlRLLAOuDIObOeJYLLxns15ldj2fsVRC8e8BbYXsJ6i3cUCvYBXIBQnea0K9qSEpkxvBWHcHb7kR-GysSMV" linktype="1" target="_blank">Read More</a></p>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <a name="LETTER.BLOCK49" class="mceItemAnchor"></a>
                                                                <table border="0" width="100%" cellspacing="0" cellpadding="0" id="content_LETTER.BLOCK49" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" valign="top" rowspan="1" colspan="1" align="left">
                                                                                <div style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;"><span style="font-family: Tahoma, Arial, sans-serif; color: #000036; font-size: 12pt;" mce_style="font-family: Tahoma, Arial, sans-serif; color: #000036; font-size: 12pt;"><strong>Putting a price on renewable capacity building</strong></span></div>
                                                                                <div style="font-family: Georgia, 'Times New Roman', Times, serif; text-align: justify;" mce_style="font-family: Georgia, 'Times New Roman', Times, serif; text-align: justify;" align="justify"><span style="font-family: Tahoma, Arial, sans-serif;" mce_style="font-family: Tahoma, Arial, sans-serif;"><img height="149" vspace="5" border="0" name="ACCOUNT.IMAGE.1830" hspace="5" width="111" src="http://ih.constantcontact.com/fs146/1102918945378/img/1830.jpg" mce_src="http://ih.constantcontact.com/fs146/1102918945378/img/1830.jpg" align="left"></span><span style="font-size: 11pt; font-family: Tahoma, Arial, sans-serif;" mce_style="font-size: 11pt; font-family: Tahoma, Arial, sans-serif;">South E</span><span style="font-size: 11pt; font-family: Tahoma, Arial, sans-serif;" mce_style="font-size: 11pt; font-family: Tahoma, Arial, sans-serif;">ast Asia has only around&nbsp;one gigawatt of clean&nbsp;energy operating asse</span><span style="font-size: 11pt; font-family: Tahoma, Arial, sans-serif;" mce_style="font-size: 11pt; font-family: Tahoma, Arial, sans-serif;">ts&nbsp;(excluding large hydro) and various&nbsp;estimates have the ASEAN group of countries with a target of&nbsp;roughly 55 gigawatts of renewable energy production by 2030. That&nbsp;translates into an investment of more than $100 billion. That from Andrew Affleck in a wide ranging interview with Yvonne Chan in the February issue of Asian Investor.&nbsp;</span>&nbsp;<a style="font-family: Tahoma, Arial, sans-serif; color: blue; text-decoration: underline;" mce_style="font-family: Tahoma, Arial, sans-serif; color: blue; text-decoration: underline;" track="on" shape="rect" mce_shape="rect" href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3TBxOCa97T-TOITlNw5VmWhTW11RzoXJpAF4r8Ld11m5Ah4Hdocd1Ob-uMACLZDskR4ke-96PyZwwnLQoyyuNaSw3wSV9k0Erv8RguzkaLbMbDUQdNZGB8c842FkdAnxDSKBgdazxURuBwSBp3pFdiOsjthW-iTdrrUbDOJijC_hZl9tYAhUbT-VpcXRuZvlx_3CAq-RYJ73GuBlvmc6ihPY45DTBeDR29UzwQr84TmuEZZeIAMFevRe6-wDJjidNaqpo6lNsJxovk-h3tRPZngELsmJnV03hwGOfDR7gmL6GpJO1OSAfframwbZdmlfEIAJgxe5ChCep9Wqjkjim6AQB3oRTvVN-A=" mce_href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3TBxOCa97T-TOITlNw5VmWhTW11RzoXJpAF4r8Ld11m5Ah4Hdocd1Ob-uMACLZDskR4ke-96PyZwwnLQoyyuNaSw3wSV9k0Erv8RguzkaLbMbDUQdNZGB8c842FkdAnxDSKBgdazxURuBwSBp3pFdiOsjthW-iTdrrUbDOJijC_hZl9tYAhUbT-VpcXRuZvlx_3CAq-RYJ73GuBlvmc6ihPY45DTBeDR29UzwQr84TmuEZZeIAMFevRe6-wDJjidNaqpo6lNsJxovk-h3tRPZngELsmJnV03hwGOfDR7gmL6GpJO1OSAfframwbZdmlfEIAJgxe5ChCep9Wqjkjim6AQB3oRTvVN-A=" linktype="1" target="_blank">Read more </a><br/></div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <a name="LETTER.BLOCK16" class="mceItemAnchor"></a>
                                                                <table style="background-color: #000036;" mce_style="background-color: #000036;" bgcolor="#000036" border="0" width="100%" cellspacing="0" cellpadding="0" id="content_LETTER.BLOCK16" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="padding:5px 8px 5px 8px;font-size: 12pt; font-family: Tahoma, Arial, sans-serif; color: rgb(255, 255, 255);" mce_style="padding:5px 8px 5px 8px;font-size: 12pt; font-family: Tahoma, Arial, sans-serif; color: #ffffff;" valign="top" rowspan="1" colspan="1" align="left">
                                                                                <div><b>Global Renewable Energy News&nbsp;</b></div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <a name="LETTER.BLOCK46" class="mceItemAnchor"></a>
                                                                <table border="0" width="100%" cellspacing="0" cellpadding="0" id="content_LETTER.BLOCK46" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" valign="top" rowspan="1" colspan="1" align="left">
                                                                                <div style="font-size: 12pt; text-align: justify; color: #000027; font-family: Tahoma, Arial, sans-serif;" mce_style="font-size: 12pt; text-align: justify; color: #000027; font-family: Tahoma, Arial, sans-serif;" align="justify"><span><strong>IPPC Report to drive capital investment in Clean Tech</strong></span></div>
                                                                                <img height="142" vspace="5" border="0" name="ACCOUNT.IMAGE.1843" hspace="5" width="125" src="http://ih.constantcontact.com/fs146/1102918945378/img/1843.jpg" mce_src="http://ih.constantcontact.com/fs146/1102918945378/img/1843.jpg" align="right">
                                                                                <div style="text-align: justify;" mce_style="text-align: justify;" align="justify"><span style="font-family: Tahoma, Arial, sans-serif; font-size: 11pt;" mce_style="font-family: Tahoma, Arial, sans-serif; font-size: 11pt;" face="Arial, Helvetica, sans-serif">The compelling conclusions from the latest United Nations report on climate change are likely to spur a&nbsp;wave of&nbsp;corporate investment, capital partnerships and bonds in the infrastructure, energy, water and agriculture sectors, James Cameron (pictured) , non-executive Chairman of Climate Change Capital, told Clean Energy Pipeline. Intergovernmental Panel on Climate Change report (31 March ) warned of an increase in violent conflicts, inequality, food shortages, droughts, flooding, heat waves and infrastructure damage if global temperatures continue to rise at current rates.&nbsp;<a style="color: rgb(0, 0, 255); text-decoration: underline;" mce_style="color: #0000ff; text-decoration: underline;" track="on" href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3ToowtZN6uVG6tLCcxNlJuiJUhJMap1TuWj6DJLvfv9pVqGeZY4hOAiFS8d5ArS15lrZ_r3iWmxxz0L616zcL8B4v-eb3KjQSovDura1LTCBGvaZd0_6YKJRZmMO_OrF70MkCs6YBAAR_sUunMIqXKwMFe_GVX-Hfr9AtFP3JEX_Ky3RbWVfUdGFhITaDSefaqnLc15416wT4b7b7riE1Q23Qu3MMQwcce9IlGw5nHUGMenqeWnBa-BKS2bLAzCahyqlXpEFrWeKlBPvzwIRhnSr6UPGLSPU5rcwP9_I8oHc_p-NuWFNRzhoeG-lKCNJxXdrX6NO82cFpDWY3ld14q-eUjkoEOUx1pxGjYaaju75w==" mce_href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3ToowtZN6uVG6tLCcxNlJuiJUhJMap1TuWj6DJLvfv9pVqGeZY4hOAiFS8d5ArS15lrZ_r3iWmxxz0L616zcL8B4v-eb3KjQSovDura1LTCBGvaZd0_6YKJRZmMO_OrF70MkCs6YBAAR_sUunMIqXKwMFe_GVX-Hfr9AtFP3JEX_Ky3RbWVfUdGFhITaDSefaqnLc15416wT4b7b7riE1Q23Qu3MMQwcce9IlGw5nHUGMenqeWnBa-BKS2bLAzCahyqlXpEFrWeKlBPvzwIRhnSr6UPGLSPU5rcwP9_I8oHc_p-NuWFNRzhoeG-lKCNJxXdrX6NO82cFpDWY3ld14q-eUjkoEOUx1pxGjYaaju75w==" shape="rect" mce_shape="rect" linktype="1" target="_blank">Read More</a>&nbsp;</span><br/></div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <a name="LETTER.BLOCK39" class="mceItemAnchor"></a>
                                                                <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content_LETTER.BLOCK39" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" valign="top" rowspan="1" colspan="1" align="left">
                                                                                <p style="margin-top: 0px; margin-bottom: 0px; font-size: 12pt; font-family: Tahoma, Arial, sans-serif; text-align: justify; color: #000036;" mce_style="margin-top: 0px; margin-bottom: 0px; font-size: 12pt; font-family: Tahoma, Arial, sans-serif; text-align: justify; color: #000036;" align="justify"><strong>Asia new global leader in new PV installations</strong></p>
                                                                                <div style="text-align: justify;" mce_style="text-align: justify;" align="justify"><img height="122" vspace="5" border="0" name="ACCOUNT.IMAGE.508" hspace="5" width="170" src="http://ih.constantcontact.com/fs085/1102918945378/img/508.jpg" mce_src="http://ih.constantcontact.com/fs085/1102918945378/img/508.jpg" align="right"></div>
                                                                                <p style="margin-top: 0px; margin-bottom: 0px; font-family: Tahoma, Arial, sans-serif; font-size: 11pt; text-align: justify;" mce_style="margin-top: 0px; margin-bottom: 0px; font-family: Tahoma, Arial, sans-serif; font-size: 11pt; text-align: justify;" align="justify">With at least 37 GW of newly-added capacity globally, 2013 w<span style="font-size: 11pt;" mce_style="font-size: 11pt;">as&nbsp;</span><span style="font-size: 11pt;" mce_style="font-size: 11pt;">another record-year </span><span style="font-size: 11pt;" mce_style="font-size: 11pt;">for photovoltaic (PV) installations, with Asia taking the lead over Europe as the number one region for new PV installations.&nbsp; This, according to preliminary figures gathered by the European Photovoltaic&nbsp; Industry Association (EPIA). </span><a style="color: blue; text-decoration: underline;" mce_style="color: blue; text-decoration: underline;" track="on" href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3SqJpYrG-MXzYKgADiGho1yDtKqxPKORECbkzicgvsfmKZ-ZDzkc_GrZn88Xcn4q4r0LPB7ihIGn8m3KytNxZNSB6-S6OxpMw1WtbHTIXtBnZJoLCy8m--7eAXDJjp7gcaQkYXy60EcWNeWUyrGfXJL5MZpipPt7jKsrW7nZu8xXiyQdHFJYgNTpel8QTSAjwrTE-NLvLK8xZZOy8PYIrSZYmxI_kbksUdGDfFmYLtgurHCEnwJALWJroIHAY0gfnGi5q6sD8TsklClWFxx00biFCuGCPhiiCvy7Hvv2hAEO059Q6CoEvix-s47rCLcMKBBIOcY8dEVz6vaVoxUCZTeLXiKxGqJ3oU=" mce_href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3SqJpYrG-MXzYKgADiGho1yDtKqxPKORECbkzicgvsfmKZ-ZDzkc_GrZn88Xcn4q4r0LPB7ihIGn8m3KytNxZNSB6-S6OxpMw1WtbHTIXtBnZJoLCy8m--7eAXDJjp7gcaQkYXy60EcWNeWUyrGfXJL5MZpipPt7jKsrW7nZu8xXiyQdHFJYgNTpel8QTSAjwrTE-NLvLK8xZZOy8PYIrSZYmxI_kbksUdGDfFmYLtgurHCEnwJALWJroIHAY0gfnGi5q6sD8TsklClWFxx00biFCuGCPhiiCvy7Hvv2hAEO059Q6CoEvix-s47rCLcMKBBIOcY8dEVz6vaVoxUCZTeLXiKxGqJ3oU=" shape="rect" mce_shape="rect" linktype="1" target="_blank">Read More</a><span style="font-size: 11pt;" mce_style="font-size: 11pt;">&nbsp;</span></p>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <a name="LETTER.BLOCK33" class="mceItemAnchor"></a>
                                                                <table style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;" border="0" width="100%" cellpadding="0" cellspacing="0" id="content_LETTER.BLOCK33" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="padding:7px 8px 7px 8px;color: rgb(0, 0, 0);" mce_style="padding:7px 8px 7px 8px;color: #000000;" valign="top" rowspan="1" colspan="1" align="left">
                                                                                <div style="color: #000000;" mce_style="color: #000000;">
                                                                                    <div><span style="font-size: 12pt;" mce_style="font-size: 12pt;"><strong>PV reaches critical mass towards mainstream power&nbsp;</strong></span><br/></div>
                                                                                    <img style="text-align: justify;" mce_style="text-align: justify;" height="102" vspace="5" name="ACCOUNT.IMAGE.1797" border="0" hspace="5" width="174" src="http://ih.constantcontact.com/fs146/1102918945378/img/1797.jpg" mce_src="http://ih.constantcontact.com/fs146/1102918945378/img/1797.jpg" align="right">
                                                                                    <div style="text-align: justify;" mce_style="text-align: justify;" align="justify"><span style="text-align: justify; font-size: 11pt;" mce_style="text-align: justify; font-size: 11pt;">Recent indicators show PV reaching a critical mass that might enable it to be&nbsp;</span><span style="text-align: justify; font-size: 11pt;" mce_style="text-align: justify; font-size: 11pt;">considered a mainstream power generation source in many parts of the world.Solar may have accounted for just 0.6% of global electricity generation in 2012, but there are signs that PV could&nbsp;</span></div>
                                                                                    <div style="text-align: justify;" mce_style="text-align: justify;" align="justify"><span style="text-align: justify; font-size: 11pt;" mce_style="text-align: justify; font-size: 11pt;">soon be entering into the mainstream.&nbsp;</span><a style="color: rgb(0, 0, 255); text-decoration: underline;" mce_style="color: #0000ff; text-decoration: underline;" track="on" href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3QV9IUUgHowqQmaGmNbi1Qe29kedx9d0tiv6U15ZY0F_caiArlCCzNYYpBhI-ZacvduXjuq4ozEBSFvo8eUS43gpHHFngw2VPcuOFUxMJGTB96kli3GbOssv2TJ9xmBxm7nQTUnuIjkNSattKnE43TkymbPVoBOeUMiJnb902flRcU5aAtkRJHIJZJIJCgeYV1lwsbY6sHXIQrszFWoKSY53O3f395mPJchsEfslzdc8iAa-NcVBUiwdes32iTcwN-9xeBstEybIW9Tt_9C9ILrWeJPk35mKcF6_J_OmcslNCV2o4T3muElZwbrFf87Ebhyp3AY-2-gKvXYpjT_yJ6ZMOheATHKba1tnYQoPQezcw==" mce_href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3QV9IUUgHowqQmaGmNbi1Qe29kedx9d0tiv6U15ZY0F_caiArlCCzNYYpBhI-ZacvduXjuq4ozEBSFvo8eUS43gpHHFngw2VPcuOFUxMJGTB96kli3GbOssv2TJ9xmBxm7nQTUnuIjkNSattKnE43TkymbPVoBOeUMiJnb902flRcU5aAtkRJHIJZJIJCgeYV1lwsbY6sHXIQrszFWoKSY53O3f395mPJchsEfslzdc8iAa-NcVBUiwdes32iTcwN-9xeBstEybIW9Tt_9C9ILrWeJPk35mKcF6_J_OmcslNCV2o4T3muElZwbrFf87Ebhyp3AY-2-gKvXYpjT_yJ6ZMOheATHKba1tnYQoPQezcw==" shape="rect" mce_shape="rect" linktype="1" target="_blank">Read More</a></div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <a name="LETTER.BLOCK40" class="mceItemAnchor"></a>
                                                                <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content_LETTER.BLOCK40" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" valign="top" rowspan="1" colspan="1" align="left">
                                                                                <div style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;text-align: justify;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;text-align: justify;" align="justify">
                                                                                    <span style="font-size: 12pt;" mce_style="font-size: 12pt;">
                                                                                        <img style="font-size: 15px;" mce_style="font-size: 15px;" height="169" vspace="5" border="0" name="ACCOUNT.IMAGE.1835" hspace="5" width="132" src="http://ih.constantcontact.com/fs146/1102918945378/img/1835.jpg" mce_src="http://ih.constantcontact.com/fs146/1102918945378/img/1835.jpg" align="right">
                                                                                        <strong>
                                                                                            <div>Wind blows back into Germany's&nbsp;"Energiewende"</div>
                                                                                        </strong>
                                                                                    </span>
                                                                                </div>
                                                                                <div style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;text-align: justify;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;text-align: justify;" align="justify">Germany's regional states succeeded in watering down the Federal Government's plans&nbsp;for cuts in future wind&nbsp;energy projects in conjunction with a landmark renewable energy law reform, Economy Minister Sigmar&nbsp;Gabriel and state leaders said early April.&nbsp;Chancellor Angela Merkel, who took part in the four-hour meeting said:&nbsp;"We've reached a high degree of unity," in getting right the country's "Energiewende", or transition to renewable energy, which will be one of the most important challenges of her right-left grand coalition government. <a style="color: rgb(0, 0, 255); text-decoration: underline;" mce_style="color: #0000ff; text-decoration: underline;" track="on" href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3T1aRvJWs04Lz173rJqXmkUrRzVX1C79bgzYCPuFlYw7-gOlAcElzgNkFCYVzSueYnx272vE59B4f_5kbyj7h7qQaD-Lg2084dM60gAMQ3yi53cCoGV38svlCIXmaII8DPQfo_-AB79USCa1LvmXj51-hqKXaRo5UUIav_FYk7TIJwaZZ7kqnCHeX961aGpMVDdW1jBZfPTdyZIjY0wfisLtQ9HgVo1IvHx9IAYIacqLLeAzeIWJ1owbtSysC0MY_XXv8egIIUhnwsrle5p97BnpuzljLt7rJwWFRiabSXfUchpKnTefYfDPoKBVFPpQTZHsVwOOgYl0gm3MS1MTwJsAT_Be0To7AE=" mce_href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3T1aRvJWs04Lz173rJqXmkUrRzVX1C79bgzYCPuFlYw7-gOlAcElzgNkFCYVzSueYnx272vE59B4f_5kbyj7h7qQaD-Lg2084dM60gAMQ3yi53cCoGV38svlCIXmaII8DPQfo_-AB79USCa1LvmXj51-hqKXaRo5UUIav_FYk7TIJwaZZ7kqnCHeX961aGpMVDdW1jBZfPTdyZIjY0wfisLtQ9HgVo1IvHx9IAYIacqLLeAzeIWJ1owbtSysC0MY_XXv8egIIUhnwsrle5p97BnpuzljLt7rJwWFRiabSXfUchpKnTefYfDPoKBVFPpQTZHsVwOOgYl0gm3MS1MTwJsAT_Be0To7AE=" shape="rect" mce_shape="rect" linktype="1" target="_blank">Read More</a></div>
                                                                                <br/>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <a name="LETTER.BLOCK48" class="mceItemAnchor"></a>
                                                                <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content_LETTER.BLOCK48" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" valign="top" rowspan="1" colspan="1" align="left">
                                                                                <div>
                                                                                    <span>
                                                                                        <div style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;text-align: justify;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;text-align: justify;" align="justify">
                                                                                            <span style="font-family: Tahoma, Arial, sans-serif; font-size: 12pt;" mce_style="font-family: Tahoma, Arial, sans-serif; font-size: 12pt;">
                                                                                                <strong>
                                                                                                    <div>Light of day for UN's Sustainable Public Procurement (SPP)&nbsp;&nbsp;&nbsp;</div>
                                                                                                </strong>
                                                                                            </span>
                                                                                            <img height="148" vspace="5" name="ACCOUNT.IMAGE.1838" border="0" hspace="5" width="148" src="http://ih.constantcontact.com/fs146/1102918945378/img/1838.png" mce_src="http://ih.constantcontact.com/fs146/1102918945378/img/1838.png" align="right">
                                                                                        </div>
                                                                                        <p style="margin-top: 0px; margin-bottom: 0px; font-family: Georgia, 'Times New Roman', Times, serif; text-align: justify;" mce_style="margin-top: 0px; margin-bottom: 0px; font-family: Georgia, 'Times New Roman', Times, serif; text-align: justify;" align="justify"><span style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;">Indian Railways replaced more than one million incandescent light bulbs with energy-efficient fluorescent lamps in 400,000 employees' homes, saving more than 100,000 megawatts of energy and reducing carbon dioxide emissions by 90,000 tonnes each year.&nbsp;</span><span style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;">An example of what can be achieved through a new United Nations Sustainable Public Procurement (SPP) global programme which was launched in New York 1 April which aims to harness the trillions of dollars that governments spend each year on procurement of everything - from computers to food to travel - to fund a more resource-efficient, low-carbon world. </span><a style="font-family: Tahoma, Arial, sans-serif; color: rgb(0, 0, 255); text-decoration: underline;" mce_style="font-family: Tahoma, Arial, sans-serif; color: #0000ff; text-decoration: underline;" track="on" href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3RSReKm3g9HRTY-NIvUL-BJd2uuy8HXe-kT4hl9aaQjmwSCdURiCcXU4aAJhb5MYoHbxerXHAgIwWYh1NjnOcohVivocsXR_nvIA3a2sW3p8Mky_k195YRgIngvp9hsbAsQmNrQgcr95N3J2Lqo2KcwQa2fL8lI2gg7Cf91VmVDV0FI_2FP6DUgfVC96bTCk4-d_H_rnCc8HMDHrTg1chZwSDjEAyy-v5Ig2AtJ1ZHOJBm1MwaDkyMZbN5M_JZl90Q59Q0IR02bql9__VZFNk2FSZAr6p_GbA10SfIYwXNwy-tCb1QQJoFlmtT5BXJwlKNB30NwYnpQrzaoLLgcZ6slGz3AQkE1-i1H_3M1WTFQEg==" mce_href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3RSReKm3g9HRTY-NIvUL-BJd2uuy8HXe-kT4hl9aaQjmwSCdURiCcXU4aAJhb5MYoHbxerXHAgIwWYh1NjnOcohVivocsXR_nvIA3a2sW3p8Mky_k195YRgIngvp9hsbAsQmNrQgcr95N3J2Lqo2KcwQa2fL8lI2gg7Cf91VmVDV0FI_2FP6DUgfVC96bTCk4-d_H_rnCc8HMDHrTg1chZwSDjEAyy-v5Ig2AtJ1ZHOJBm1MwaDkyMZbN5M_JZl90Q59Q0IR02bql9__VZFNk2FSZAr6p_GbA10SfIYwXNwy-tCb1QQJoFlmtT5BXJwlKNB30NwYnpQrzaoLLgcZ6slGz3AQkE1-i1H_3M1WTFQEg==" shape="rect" mce_shape="rect" linktype="1" target="_blank">Read More</a></p>
                                                                                    </span>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <a name="LETTER.BLOCK42" class="mceItemAnchor"></a>
                                                                <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content_LETTER.BLOCK42" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" valign="top" rowspan="1" colspan="1" align="left">
                                                                                <img height="183" vspace="5" name="ACCOUNT.IMAGE.958" border="0" hspace="5" width="170" src="http://ih.constantcontact.com/fs085/1102918945378/img/958.jpg" mce_src="http://ih.constantcontact.com/fs085/1102918945378/img/958.jpg" align="right">
                                                                                <p style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;margin-top: 0px; margin-bottom: 0px; text-align: justify;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;margin-top: 0px; margin-bottom: 0px; text-align: justify;" align="justify"><span style="font-size: 12pt;" mce_style="font-size: 12pt;"><strong>Africa: Dedicated Fund for Small to Medium Renewable Projects</strong></span></p>
                                                                                <p style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;margin-top: 0px; margin-bottom: 0px; text-align: justify;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;margin-top: 0px; margin-bottom: 0px; text-align: justify;" align="justify">The African Renewable Energy Fund (AREF), a dedicated renewable energy fund focused on Sub-Saharan Africa closed on 12 March with US$100 million of committed capital to support small- to medium-scale independent power producers (IPPs). Headquartered in Nairobi,&nbsp; the fund is targeting a final close of US $200 million within the next 12 months to be invested in grid-connected development stage renewable energy projects including small hydro, wind, geothermal, solar, biomass and waste gas. <a style="color: rgb(0, 0, 255); text-decoration: underline;" mce_style="color: #0000ff; text-decoration: underline;" track="on" href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3Rmlsx8Kuwd1IYcUOSaFgUfOEosK_pWNwc3xotrUXUwcHW4ZlyxA9vNi8i9XgJucI08QUvC-LoKpb3QECw0fzypT7B4Fe6Ldi969O3Q1ovX-4IoWaP6Xyf-nzxcwrQu9WkBvJIxjyYM1UltQIIZ5t168bCTo3gcbIiTdxiDiUjTFpbKp1pIQTmNbPZklmkloMTlhdoCURlC5eblgfUO7T6eJuwSaxJgNgnY774ULx4z2SEu5UU2c6eNGB6UW9FBwaNh38jrCzthZlp6xxN1c1w7LoqS0nXW2Iugq2HrHucfPx11tvpf64LrIadGm5o2XM2v5AJZL3YRsxGYIwMaDZet9AaYhs2s_bJ7xq5El-sSrgCcChInS0Rq" mce_href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3Rmlsx8Kuwd1IYcUOSaFgUfOEosK_pWNwc3xotrUXUwcHW4ZlyxA9vNi8i9XgJucI08QUvC-LoKpb3QECw0fzypT7B4Fe6Ldi969O3Q1ovX-4IoWaP6Xyf-nzxcwrQu9WkBvJIxjyYM1UltQIIZ5t168bCTo3gcbIiTdxiDiUjTFpbKp1pIQTmNbPZklmkloMTlhdoCURlC5eblgfUO7T6eJuwSaxJgNgnY774ULx4z2SEu5UU2c6eNGB6UW9FBwaNh38jrCzthZlp6xxN1c1w7LoqS0nXW2Iugq2HrHucfPx11tvpf64LrIadGm5o2XM2v5AJZL3YRsxGYIwMaDZet9AaYhs2s_bJ7xq5El-sSrgCcChInS0Rq" shape="rect" mce_shape="rect" linktype="1" target="_blank">Read More</a></p>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <a name="LETTER.BLOCK41" class="mceItemAnchor"></a>
                                                                <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content_LETTER.BLOCK41" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" valign="top" rowspan="1" colspan="1" align="left">
                                                                                <div>
                                                                                    <span>
                                                                                        <span style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;">
                                                                                            <strong>
                                                                                                <div><span style="font-size: 12pt;" mce_style="font-size: 12pt;">Creating an energy future sustainable for&nbsp;all</span> <img height="149" vspace="5" border="0" name="ACCOUNT.IMAGE.1784" hspace="5" width="160" src="http://ih.constantcontact.com/fs146/1102918945378/img/1784.png" mce_src="http://ih.constantcontact.com/fs146/1102918945378/img/1784.png" align="right"></div>
                                                                                            </strong>
                                                                                        </span>
                                                                                        <div style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;text-align: justify;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;text-align: justify;" align="justify"><span style="text-align: justify;" mce_style="text-align: justify;">Action one in the outcome document of the Asian Productivity Organisation&nbsp;</span><span style="text-align: justify;" mce_style="text-align: justify;">inter</span><span style="text-align: justify;" mce_style="text-align: justify;">national conference in Taipei mid-March called for targets to&nbsp;</span><span style="text-align: justify;" mce_style="text-align: justify;">be set for cities and&nbsp;</span><span style="text-align: justify;" mce_style="text-align: justify;">count</span><span style="text-align: justify;" mce_style="text-align: justify;">ries in the Asia Pacific to meet at least 20% of energy&nbsp;demand&nbsp;from renewable sources by&nbsp;2020 and to achieve&nbsp;up to&nbsp;20% improvement in energy efficiency across the board by&nbsp;2020. </span><a style="color: rgb(0, 0, 255); text-decoration: underline;" mce_style="color: #0000ff; text-decoration: underline;" track="on" href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3SfOsNbf7nWhGBZgiveUZUT6xHekjuTo7UMGWLdSibOJyE1ezIugCx-gJ11qosWnlFnG0Yf7VRWOsEvrUW6BWQ3iT7j0_PmYVS8BAA1Ls1K38qi7Di1_ol9Fjye-SUkL_0W0pMRwf_sn2F6bW9pqyBUoKLs8smgss7FHw5I3xbFWL3Z2Xlphu8LNY9twZMnZdAJLcSUZW0u8Qbt57NW804_zZYXFEC3-Vvaej47XZN45YO_k21yyl69slUk9xVcxzL4BcWBMdzG5meeOVEMvWu29goheb0uNOlfdXP4-qJmqay3GXEXpsNy-Vtc1ZGWQWGCr-_45KOnV3ZeRjHb1l_-PJsXUZgO1aE=" mce_href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3SfOsNbf7nWhGBZgiveUZUT6xHekjuTo7UMGWLdSibOJyE1ezIugCx-gJ11qosWnlFnG0Yf7VRWOsEvrUW6BWQ3iT7j0_PmYVS8BAA1Ls1K38qi7Di1_ol9Fjye-SUkL_0W0pMRwf_sn2F6bW9pqyBUoKLs8smgss7FHw5I3xbFWL3Z2Xlphu8LNY9twZMnZdAJLcSUZW0u8Qbt57NW804_zZYXFEC3-Vvaej47XZN45YO_k21yyl69slUk9xVcxzL4BcWBMdzG5meeOVEMvWu29goheb0uNOlfdXP4-qJmqay3GXEXpsNy-Vtc1ZGWQWGCr-_45KOnV3ZeRjHb1l_-PJsXUZgO1aE=" shape="rect" mce_shape="rect" linktype="1" target="_blank">Read More</a></div>
                                                                                        <div>&nbsp;</div>
                                                                                    </span>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <a name="LETTER.BLOCK36" class="mceItemAnchor"></a>
                                                                <table style="background-color: #cbcbcb;" mce_style="background-color: #cbcbcb;" bgcolor="#CBCBCB" border="0" width="100%" cellspacing="0" cellpadding="0" id="content_LETTER.BLOCK36" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="background-color:#000000;padding:5px 8px 5px 8px;font-size: 12pt; font-family: Tahoma, Arial, sans-serif; color: rgb(255, 255, 255);" mce_style="background-color:#000000;padding:5px 8px 5px 8px;font-size: 12pt; font-family: Tahoma, Arial, sans-serif; color: #ffffff;" bgcolor="#000000" valign="top" rowspan="1" colspan="1" align="left">
                                                                                <div><b>Featuring Clean Energy Applied Research: SERIS &nbsp;</b></div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding:7px 8px 11px 8px;font-size: 11pt; font-family: Georgia, 'Times New Roman', Times, serif; color: rgb(0, 0, 0);" mce_style="padding:7px 8px 11px 8px;font-size: 11pt; font-family: Georgia, 'Times New Roman', Times, serif; color: #000000;" valign="top" rowspan="1" colspan="1" align="left">
                                                                                <div>
                                                                                    <p style="margin-top: 0px; margin-bottom: 0px; font-size: 11pt; font-family: Georgia, 'Times New Roman', Times, serif;" mce_style="margin-top: 0px; margin-bottom: 0px; font-size: 11pt; font-family: Georgia, 'Times New Roman', Times, serif;"><span style="font-size: 12pt;" mce_style="font-size: 12pt;"><strong><strong><strong><strong><strong><strong><strong><strong><strong> <img height="124" vspace="5" name="ACCOUNT.IMAGE.1831" border="0" hspace="5" width="223" src="http://ih.constantcontact.com/fs146/1102918945378/img/1831.jpg" mce_src="http://ih.constantcontact.com/fs146/1102918945378/img/1831.jpg" align="left"></strong></strong></strong></strong></strong></strong></strong></strong></strong></span></p>
                                                                                    <div style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;">
                                                                                        <span>
                                                                                            <p style="margin-top: 0px; margin-bottom: 0px;" mce_style="margin-top: 0px; margin-bottom: 0px;">What started as a research project by the Solar Energy Research Institute of Singapore (SERIS) at National University of Singapore (NUS) has culminated into a spin-off that helps light up rural communities.&nbsp;With a grant from the Singapore-MIT Alliance for Research and Technology (SMART), the team initiated the concept of affordable solar-powered street lamps that can be used in areas not connected to an electricity grid. The development of the idea led to the formation of start-up&nbsp;<a style="color: rgb(0, 0, 0);" mce_style="color: #000000;" shape="rect" mce_shape="rect">Fosera Lighting Pte Ltd</a>&nbsp;to commercialise the product.&nbsp;<a style="color: rgb(0, 0, 255); text-decoration: underline;" mce_style="color: #0000ff; text-decoration: underline;" track="on" href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3SuFGPye1N1lFDBS__R264rqGow8er3HhOmGQqlgeYRxzHVUjtgCeiaVTUBniWNoshLwFMnnf7Nb1FLijSc7HkPDCjGjzTBSuttDCwE0Sl88GUE_HvmTSaCLTFVzoD1HWT9Bbp9anNKGabmQTc0diFwcVqr8dQMAUvpFQhOaXGhJ_5PWyTqpsKZhd5tuj-Lkzu6-0R4rSHQicDF4NDg0WM6ow7LXZN3tjXh3uJBJd1NQhTGJodhQvxnoGC-LtxljD9YbH33VjWBQo5xagf9QRjKor-yUecvCBv_JHA4d2O42kqc37I2U0YI91wd-NuEVbpHJqKuHAV4h8LkDr3ugSF7QeA6h4hPJng=" mce_href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3SuFGPye1N1lFDBS__R264rqGow8er3HhOmGQqlgeYRxzHVUjtgCeiaVTUBniWNoshLwFMnnf7Nb1FLijSc7HkPDCjGjzTBSuttDCwE0Sl88GUE_HvmTSaCLTFVzoD1HWT9Bbp9anNKGabmQTc0diFwcVqr8dQMAUvpFQhOaXGhJ_5PWyTqpsKZhd5tuj-Lkzu6-0R4rSHQicDF4NDg0WM6ow7LXZN3tjXh3uJBJd1NQhTGJodhQvxnoGC-LtxljD9YbH33VjWBQo5xagf9QRjKor-yUecvCBv_JHA4d2O42kqc37I2U0YI91wd-NuEVbpHJqKuHAV4h8LkDr3ugSF7QeA6h4hPJng=" shape="rect" mce_shape="rect" linktype="1" target="_blank">Read More</a></p>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table style="background:transparent;" mce_style="background:transparent;" border="0" width="100%" cellspacing="0" cellpadding="0" class="mceItemTable">
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding:0px 12px 0px 12px;" mce_style="padding:0px 12px 0px 12px;" valign="top" width="100%" rowspan="1" colspan="1" align="center">
                                                                <a name="LETTER.BLOCK34" class="mceItemAnchor"></a>
                                                                <table style="background-color:#555555;" mce_style="background-color:#555555;" bgcolor="#555555" border="0" width="100%" cellpadding="0" cellspacing="0" id="content_LETTER.BLOCK34" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="color:#FFFFFF;font-family:Tahoma,Arial,sans-serif;font-size:12pt;padding:5px 8px 5px 8px;" mce_style="color:#FFFFFF;font-family:Tahoma,Arial,sans-serif;font-size:12pt;padding:5px 8px 5px 8px;" valign="top" rowspan="1" colspan="1" align="left">Contact Us</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <table border="0" width="100%" cellspacing="0" cellpadding="0" id="content_LETTER.BLOCK28" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" mce_style="color:#000000;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:7px 8px 7px 8px;" valign="top" rowspan="1" colspan="1" align="center">
                                                                                <img height="47" vspace="5" border="0" name="ACCOUNT.IMAGE.1833" hspace="5" width="133" src="http://ih.constantcontact.com/fs146/1102918945378/img/1833.jpg" mce_src="http://ih.constantcontact.com/fs146/1102918945378/img/1833.jpg" align="right">
                                                                                <div style="text-align: left; color: #001a81; font-family: Tahoma, Arial, sans-serif; font-size: 12pt;" mce_style="text-align: left; color: #001a81; font-family: Tahoma, Arial, sans-serif; font-size: 12pt;" align="left"><strong>Armstrong Asset Management&nbsp;</strong></div>
                                                                                <div style="text-align: left; color: #001a81; font-family: Tahoma, Arial, sans-serif; font-size: 12pt;" mce_style="text-align: left; color: #001a81; font-family: Tahoma, Arial, sans-serif; font-size: 12pt;" align="left"><span><strong>30 Raffles Place</strong></span></div>
                                                                                <div style="text-align: left; color: #001a81; font-family: Tahoma, Arial, sans-serif; font-size: 12pt;" mce_style="text-align: left; color: #001a81; font-family: Tahoma, Arial, sans-serif; font-size: 12pt;" align="left"><strong>#10-04 Chevron House</strong><br/></div>
                                                                                <div style="text-align: left; color: #001a81; font-family: Tahoma, Arial, sans-serif; font-size: 12pt;" mce_style="text-align: left; color: #001a81; font-family: Tahoma, Arial, sans-serif; font-size: 12pt;" align="left"><strong>Singapore 048622</strong><br/></div>
                                                                                <div style="text-align: left; color: #001a81; font-family: Tahoma, Arial, sans-serif; font-size: 12pt;" mce_style="text-align: left; color: #001a81; font-family: Tahoma, Arial, sans-serif; font-size: 12pt;" align="left"><strong>Telephone +65 6922 9790</strong></div>
                                                                                <div style="text-align: left; color: #001a81; font-family: Tahoma, Arial, sans-serif; font-size: 12pt;" mce_style="text-align: left; color: #001a81; font-family: Tahoma, Arial, sans-serif; font-size: 12pt;" align="left"><img style="color: #000000; font-size: 15px; text-align: -webkit-center;" mce_style="color: #000000; font-size: 15px; text-align: -webkit-center;" height="58" vspace="5" border="0" name="ACCOUNT.IMAGE.1834" hspace="5" width="100" src="http://ih.constantcontact.com/fs146/1102918945378/img/1834.jpg" mce_src="http://ih.constantcontact.com/fs146/1102918945378/img/1834.jpg" align="right"></div>
                                                                                <br/><a class="imgCaptionAnchor" track="on" shape="rect" mce_shape="rect" href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3SW8fbnLyD1zRJo7cRKB-nHVwPGpyFPnMaepcjdCG8lkfbXNLBMYJUiZEyD8rUUi0-2ebx1qDiRmjJSUbOAqS_zOKqrmuh1_VK6-yvQDX0Y-d52LxUOx5Af" mce_href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3SW8fbnLyD1zRJo7cRKB-nHVwPGpyFPnMaepcjdCG8lkfbXNLBMYJUiZEyD8rUUi0-2ebx1qDiRmjJSUbOAqS_zOKqrmuh1_VK6-yvQDX0Y-d52LxUOx5Af" target="_blank"><img height="30" vspace="5" border="0" name="ACCOUNT.IMAGE.1847" hspace="5" width="30" src="http://ih.constantcontact.com/fs146/1102918945378/img/1847.png" mce_src="http://ih.constantcontact.com/fs146/1102918945378/img/1847.png" align="left"></a> <a class="imgCaptionAnchor" track="on" shape="rect" mce_shape="rect" href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3TMdBosw-qk3wI0W67jjxpVgNnPkZKK4ZfGZflSUMoIC4n2Cm0P_oEsONoNo3Kf7QV5UeNQyntHAZ1UdvWL4tykNdrUaeGqedO-sAkRD9_10Mi5F5qRgT8oc2Go0UhV5DxHlM1nHikkRL-Kr2uo-jguMDDZDu8Xn18=" mce_href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3TMdBosw-qk3wI0W67jjxpVgNnPkZKK4ZfGZflSUMoIC4n2Cm0P_oEsONoNo3Kf7QV5UeNQyntHAZ1UdvWL4tykNdrUaeGqedO-sAkRD9_10Mi5F5qRgT8oc2Go0UhV5DxHlM1nHikkRL-Kr2uo-jguMDDZDu8Xn18=" target="_blank"><img height="30" vspace="5" border="0" name="ACCOUNT.IMAGE.1848" hspace="5" width="30" src="http://ih.constantcontact.com/fs146/1102918945378/img/1848.png" mce_src="http://ih.constantcontact.com/fs146/1102918945378/img/1848.png" align="left"></a> <br/>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table style="background:transparent;" mce_style="background:transparent;" border="0" width="100%" cellspacing="0" cellpadding="0" class="mceItemTable">
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding:0px 0px 0px 0px;" mce_style="padding:0px 0px 0px 0px;" valign="middle" width="100%" rowspan="1" colspan="1" align="center">
                                                                <table border="0" width="100%" cellspacing="0" cellpadding="0" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="padding-bottom:11px;height:1px;line-height:1px;" height="1" rowspan="1" colspan="1" align="center"><img height="1" vspace="0" border="0" hspace="0" width="5" style="display: block;" mce_style="display: block;" alt="" src="http://img.constantcontact.com/letters/images/1101116784221/S.gif" mce_src="http://img.constantcontact.com/letters/images/1101116784221/S.gif"></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <table style="background-color:#000000;" mce_style="background-color:#000000;" bgcolor="#000000" border="0" width="100%" cellspacing="0" cellpadding="0" id="content_LETTER.BLOCK30" class="mceItemTable">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="color:#FFFFFF;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:5px 8px 5px 8px;" mce_style="color:#FFFFFF;font-family:Tahoma,Arial,sans-serif;font-size:11pt;padding:5px 8px 5px 8px;" valign="top" rowspan="1" colspan="1" align="center">
                                                                                <div><span style="text-decoration: underline;" mce_style="text-decoration: underline;"><a style="color: rgb(255, 255, 255); text-decoration: underline;" mce_style="color: #ffffff; text-decoration: underline;" track="on" shape="rect" mce_shape="rect" href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3RRiv610f9QH068Lproxhf9ANOhu19B5uG9jhPc7GfvTUcWlNER3GuJuRoRMpVDzTwzBAYWFRvd9NYu-8PgCQ9Z4TqzjXzt2QcVypsdXDYCg2ithfg-7rsrm5qym4P0ICWFqkIri7CEA2OHh-zWGemzfadgJsZe6Yeuk4fG9xpFDTHWPt9MiKKc0MI7ESToNw0Z8bfTfzZ7OyQa6ek0CCJnlk1Noiv5i1wp3m1pLjV1eSkCAOZG3I9-jwdWeDvMyzecwV1-fJqI1XEDqkSFlxwPgym6d-SyzcwU26fkeEqFgw==" mce_href="http://r20.rs6.net/tn.jsp?e=001KzJpPk6ro3RRiv610f9QH068Lproxhf9ANOhu19B5uG9jhPc7GfvTUcWlNER3GuJuRoRMpVDzTwzBAYWFRvd9NYu-8PgCQ9Z4TqzjXzt2QcVypsdXDYCg2ithfg-7rsrm5qym4P0ICWFqkIri7CEA2OHh-zWGemzfadgJsZe6Yeuk4fG9xpFDTHWPt9MiKKc0MI7ESToNw0Z8bfTfzZ7OyQa6ek0CCJnlk1Noiv5i1wp3m1pLjV1eSkCAOZG3I9-jwdWeDvMyzecwV1-fJqI1XEDqkSFlxwPgym6d-SyzcwU26fkeEqFgw==" linktype="1" target="_blank">www.armstrongam.com</a></span></div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
					        </div>
						</div>
					</div>

					<span class="content-close" id="content-close" onclick="content_close()"></span>
				</div>
      </div>
      <div class="clearfix"></div>
    </div>
  

 	<?php
    include ('footer.php');
  ?>

  <!-- Jquery -->
  <script src="js/armstrongam.js"></script>

  </body>
</html>
