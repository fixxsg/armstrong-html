<?php
$page = "contact";
    include ('header.php');
?>
        
        <div id="wrap-container" class="container">
            <!-- <div class="row"> -->
                <div id="wrap-sidebar" class=" col-xs-12 standard-wid">
                    <div id="sidebar">
                        <div class="investment_portfolio item">
                            <div class="stitle">Contact</div>
                        </div>
                        <div class="business contact-business item" data-flag="contact" data-id="#item-content-1">
                            <div class="bdescription">
                                For general enquiries, please contact: <br>
                                <strong style="color:#419341"> Armstrong Asset Management</strong><br>
                                30 Raffles Place #10-04 Chevron House <br>
                                Singapore 048622
                            </div>
                            <a href="#" class="sviewmore"></a>
                        </div>
                        <div class="item item-contact">
                            <div class="bdescription">
                                <div class="left">
                                    Contact <br>
                                    Phone
                                </div>
                                <div class="right">
                                    Percilia Thiah<br>
                                    +65 6922 9790
                                </div>
                            </div>
                            <div class="bdescription"> 
                                <strong>General Enquiries</strong><br>
                                <script TYPE="text/javascript">
                                  emailE=('in' + 'fo' + '@' + 'armstrongam.com')  
                                  document.write(
                                    '<a style="color:#419341" href="mailto:' + emailE + '">' + emailE + '</a>'
                                  )
                                </script>
                                <NOSCRIPT>
                                  <em>
                                    This e-mail address is being protected from spambots.<br>
                                    You need JavaScript enabled to view it.
                                  </em>
                                </NOSCRIPT>
                            </div>
                            <div class="bdescription"> 
                                <strong>Business Plan Submission</strong><br>
                                <script TYPE="text/javascript">
                                  emailE=('biz' + 'plans' + '@' + 'armstrongam.com')  
                                  document.write(
                                    '<a style="color:#419341" href="mailto:' + emailE + '">' + emailE + '</a>'
                                  )
                                </script>
                                <NOSCRIPT>
                                  <em>
                                    This e-mail address is being protected from spambots.<br>
                                    You need JavaScript enabled to view it.
                                  </em>
                                </NOSCRIPT>
                            </div>
                        </div>
                        <div class="item">
                            <div class="bdescription">
                                <a href="disclaimer.php">Legal Disclaimer</a>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- </div>
            <div class="row"> -->
                <div id="wrap-content" style="padding:0px;">
                    <div class="item-content" id="item-content-1" style="margin:0px">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7977.637272099668!2d103.85106450311972!3d1.2826305296214764!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da1909351f4293%3A0xce3e2480ec0629d!2sChevron+House%2C+30+Raffles+Pl%2C+Singapore+048622!5e0!3m2!1sen!2s!4v1407988469271" width="100%" height="100%" frameborder="0" style="border:0px"></iframe>
                    </div>
                    <span class="content-close" id="content-close" onclick="content_close('contact')"></span>
                </div>
            <!-- </div> -->
            <!-- <div class="clearfix"></div> -->
        </div>
    <!-- </div> -->

    <?php
        include ('footer.php');
    ?>

    <!-- Jquery -->
    <script src="js/armstrongam.js"></script>

    </body>
</html>
