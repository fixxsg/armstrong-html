<?php
    if (isset($_GET["id"])) $id = $_GET["id"];
    else                    $id = null;

    $page = "aboutus";
    include ('header.php');
?>

        <div id="wrap-container" class="container">
            <!-- <div class="row"> -->
                <div id="wrap-sidebar" class="col-xs-12 standard-wid">
                    <div id="sidebar">
                        <!-- <div class="investment_portfolio item">
                            <div class="stitle">ABOUT US</div>
                        </div> -->

                        <?php 
                            if($id == null || $id == 1 || $id > 2)
                                echo '<div class="business item" data-id="#item-content-1">';
                            else 
                                echo '<div class="business item faded" data-id="#item-content-1">';
                        ?>
                            <div class="bdescription image-about-us">
                                <img height="32px" src="">
                            </div>
                            <a href="#" class="sviewmore"></a>
                        </div>
                        
                        <div class="business item <?php if($id != 2) echo 'faded'?>" data-id="#item-content-2">
                            <div class="bdescription">
                                The Team
                            </div>
                            <a href="#" class="sviewmore"></a>
                        </div>
                        <!--
                        <div class="business item" data-id="#item-content-3">
                            <div class="bdescription">
                                Expert Panel
                            </div>
                            <a href="#" class="sviewmore"></a>
                        </div>
                        -->
                    </div>
                </div>
            <!-- </div>
            <div class="row"> -->
                <div id="wrap-content">
                    <div id="content-cover" class="col-sm-12 col-md-12 content-cover">

                        <?php 
                            if($id == null || $id == 1 || $id > 2)
                                echo '<div class="item-content" id="item-content-1">';
                            else 
                                echo '<div class="item-content hidden" id="item-content-1">';
                        ?>
                            <h1>
                                <img height="32px" src="images/logo-mobile.png">
                            </h1>
                            <br/>
                            <p>Armstrong Asset Management is an independent clean energy asset manager, committed to investing into clean energy infrastructure assets that leave a long term positive impact on society and the natural environment.</p>
                            <p>Lord Armstrong, born in England in 1810, was a pioneer of the industrial revolution, inventor of hydraulic systems that enabled heavy lifting in the manufacture of ships, bridges and railways.</p>
                            <p>At a time when the world’s leading economies had their success built on the access to coal, he foresaw that due to a finite resource availability, a transition would be required towards renewable forms of energy within two centuries.  In a speech he made in 1863 as the President of the British Association for the Advancement of Science, he began advocating the use of hydroelectricity, and also supported solar power, stating that the solar energy received by 1-acre (4,000 m2) in tropical areas would "exert the amazing power of 4000 horses acting for nearly nine hours every day.”</p>
                            <p>150 years later we believe that his vision is being delivered. We see that clean energy technologies have matured to a point where they are cost competitive with fossil fuels in many markets. This is the trigger point for the widespread deployment of clean energy infrastructure, which will begin in markets where issues of energy security and reliability are uppermost and where the demand for power is growing.</p>
                            <p>Not only was Armstrong a visionary, but he was an engineer who delivered practical solutions. His country home at Cragside (Northumberland, UK) was the first house in the world to be powered by hydroelectric power. Today, Armstrong Asset Management is investing globally in infrastructure assets that deliver clean power to local communities and businesses, in line with a vision outlined 150 years ago by Lord Armstrong.</p>
                        </div>
                        
                        <div class="item-content <?php if($id != 2) echo 'hidden'?>" id="item-content-2">
                            <h1>THE TEAM</h1>
                            <br/>
                            <div class="col-sm-6 col-md-4 col-lg-3 tooltips">
                                <div class="cv-tooltip">
                                    <img width="90%" src="images/team/andrew_affleck.jpg" />
                                    <div class="arrow right"></div>
                                    <div class="img-tooltip right">
                                        <div class="tooltip-content">
                                            Andrew is the Managing Partner of Armstrong Asset Management. Andrew was formally the CEO of Low Carbon Investors Ltd, a dedicated global clean energy fund management group with over US$300 million under management. During this four year tenure, he co-led the transformation of the firm from a US$50 million single fund cleantech venture business to a multi-fund clean energy infrastructure asset manager. The infrastructure funds have led to operational experience being gained through the development and acquisition of 28 small scale (sub 10MW) solar and wind projects which in aggregate amount to 83MW. Prior to joining Low Carbon Investors, Andrew was responsible for fund management at Devonshire Capital, a firm he co-founded in Asia with partners in 1995. In all he has 22 years of asset management and investment banking experience in the Asian region, and has specialized in clean energy investments for the last 7 years. Andrew holds an MBA in Finance from Leicester University and a PGCert in Climate Change and Sustainable Development from De Montfort University.
                                        </div>
                                    </div>
                                </div>
                                <p>Andrew Affleck<br/>Managing Partner</p>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 tooltips">
                                <div class="cv-tooltip">
                                    <img width="90%" src="images/team/michael_mcneill.jpg">
                                    <div class="arrow right"></div>
                                    <div class="img-tooltip right">
                                        <div class="tooltip-content">
                                            classMichael has 24 years of relevant legal and practical experience in carrying out due diligence, structuring, negotiating and closing transactions in the Energy and Infrastructure sectors, acting for a wide range of stakeholders including infrastructure funds and venture partners, project developers, lenders, governments, multilaterals/donors, and EPC, O&M and other contractors/service providers. Michael has 16 years experience based in Singapore working on Energy and Infrastructure projects throughout the region, including 10 years experience as a Partner of Baker McKenzie where he headed the Regional Energy and Infrastructure Practice Group. During his time in Asia, he has been involved in over 100 transactions in the Energy (including renewables) and Infrastructure (including water) sectors covering both green-field and expansion projects, and brown-field acquisitions and divestments. He has leading expertise in conducting risk analysis (identifying key project risks and how these may be managed and mitigated) and has substantial knowledge of the reasons for failed projects gained from participating in "work-outs" (being based in the region both before and after the Asian financial crisis). Michael has structured transactions in both negotiated and competitive bid situations, and has worked on closed transactions in Asia exceeding US$30 billion in value. Michael started his career at the largest law firm in Scotland and then worked in London from 1989 to 1995, first in the corporate commercial group at a City firm, and then as in-house counsel at Esso UK Plc. Michael has an LLB (honours) from the University of Dundee where he attended the Centre for Petroleum and Natural Resources Law. He is admitted to practice law in England and Wales.
                                        </div>
                                    </div>
                                </div>
                                <p>Michael M. McNeill<br/>Partner</p>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 tooltips">
                                <div class="cv-tooltip">
                                    <img width="90%" src="images/team/edward_douglas.jpg" >
                                    <div class="arrow left"></div>
                                    <div class="img-tooltip left">
                                        <div class="tooltip-content">
                                            Edward has more than 20 years’ experience as an engineer and investment professional in the renewable energy, power generation and utilities sectors. Prior to joining Armstrong Asset management, he was the Senior Investment Director with FE Clean Energy Group Inc., a leading private equity fund manager specializing in renewable energy which, between 2006 and 2011, closed investments in Asia totalling >$150 m. He has Board experience across a variety of active companies and, from 2007 – 2011, served as Managing Director of an integrated bio-ethanol and bio-energy agriculture operation based in Thailand. Edward previously held management positions with Siemens, Cummins Power and SP International, the power investment arm of Temasek Holdings, where he managed the development of natural gas power and energy efficiency projects across the Asia Pacific region. He combines applied understanding of a wide range of energy technologies with hands-on experience of leading the engineering, plant construction and operations of small energy businesses in Asia. He is a Chartered Engineer and holds a BSc Honors degree in Mechanical Engineering from the University of Dundee and an MBA from Imperial College, London.
                                        </div>
                                    </div>
                                </div>
                                <p>Edward Douglas<br/>Partner</p>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 tooltips">
                                <div class="cv-tooltip">
                                    <img width="90%" src="images/team/yasushi.jpg" >
                                    <div class="arrow left"></div>
                                    <div class="img-tooltip left">
                                        <div class="tooltip-content">
                                            Yasushi has 17 years’ experience as an engineer and investment professional in the water, power generation and energy efficiency sectors across Europe and Asia. Prior to joining Armstrong Asset Management, he served successively as the Business Development Director for Asia (2004-2010) at the Dalkia Group, the energy division of Veolia Environnement, and as the Investment Director (2010-2013) at Swiss-Asia Financial Services Pte Ltd., manager of the China District Energy Fund. He closed investments in Asia totaling USD 240 million between 2007 and 2011. At Dalkia, he also spearheaded business development in new markets in North East and South East Asia in the field of energy efficiency, district energy and biomass CHP. Yasushi previously held a senior management position with Degrémont, the water treatment division of Suez Environnement, at its world headquarters in Paris where he headed the technical department providing internal technical assistance to EPC and O&M activities of the company all over the world. He combines a strong technical background in environmental technologies with hands-on experience in origination, development, execution and operation of energy projects in Asia. He holds a BSc and MEng degree in Urban & Environmental Engineering from the University of Tokyo and an MBA from INSEAD.
                                        </div>
                                    </div>
                                </div>
                                <p>Yasushi Ujioka<br/>Investment Director</p>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 tooltips">
                                <div class="cv-tooltip">
                                    <img width="90%" src="images/team/eliza_foo.jpg" >
                                    <div class="arrow right"></div>
                                    <div class="img-tooltip right">
                                        <div class="tooltip-content">
                                            Prior to joining Armstrong Asset Management, Eliza spent three and half years working for Carlyle Asia Partners, focusing on private equity opportunities in Southeast Asia and across various industries based in Singapore. Prior to that, Eliza worked in Investment Banking for four years at both Merrill Lynch and Goldman Sachs, specializing in corporate finance advisory and Natural Resources respectively. She started her career at Accenture, accumulating two years of consulting experience. Eliza received her Masters of Applied Finance from the University of Melbourne and Bachelor of Commerce (majoring in Finance, Accounting and Management) from the Australian National University.
                                        </div>
                                    </div>
                                </div>
                                <p>Eliza Foo<br/>Senior Investment Manager</p>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 tooltips">
                                <div class="cv-tooltip">
                                    <img width="90%" src="images/team/wymen_chan.jpg" >
                                    <div class="arrow right"></div>
                                    <div class="img-tooltip right">
                                        <div class="tooltip-content">
                                            Wymen has 8 years of experience in corporate finance and private equity and has been involved in all aspects of due diligence, deal structuring and execution. He has spent the better part of the last 15 years in the United States and has 5 years of working experience in U.S. private equity funds. He is fluent in English and Mandarin. Prior to joining Armstrong Asset Management, Wymen worked in a similar capacity at Maybank MEACP for 2 years where he focused on investments in the clean energy sector across South East Asia. Prior to that, Wymen was an Associate at Leeds Equity Partners in New York for 3 years where he focused on leveraged buyouts in the education, business process outsourcing and consumer goods sectors. Wymen received his Masters of International Affairs (Finance) from the Columbia University and Bachelor of Arts (majoring in Economics and English Literature) and Bachelor of Science (Applied Mathematics) from the University of Texas at Austin.
                                        </div>
                                    </div>
                                </div>
                                <p>Wymen Chan<br/>Senior Investment Manager</p>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 tooltips">
                                <div class="cv-tooltip">
                                    <img width="90%" src="images/team/sabine_chalopin.jpg" >
                                    <div class="arrow left"></div>
                                    <div class="img-tooltip left">
                                        <div class="tooltip-content">
                                            Sabine has worked in the clean energy industry in both Southeast Asia and London since 2004. Previously, at Kyoto Energy Pte Ltd, a Clean Development Mechanism project consultant based in Singapore, Sabine was responsible for business development for projects in the region. Prior to moving to Singapore, Sabine was part of the early team at Berkeley Energy, managers to the Renewable Energy Assets Fund (REAF), investing private equity in energy infrastructure assets in Asia. At REAF, she worked on analysing and assessing the opportunity for investments in renewable energy projects in Asia. Sabine started her career at Carbon Capital Markets, a vertically integrated carbon player, where she worked on the sales and trading desk and subsequently in a capital raising role. Sabine has an MSc in Environmental Technology from Imperial College London and a BCom in Business Studies from Edinburgh University.
                                        </div>
                                    </div>
                                </div>
                                <p>Sabine Chalopin<br/>Investment Manager, ESG Officer</p>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 tooltips">
                                <div class="cv-tooltip">
                                    <img width="90%" src="images/team/jason_wang.jpg" >
                                    <div class="arrow left"></div>
                                    <div class="img-tooltip left">
                                        <div class="tooltip-content">
                                            Jason has 11 years of experience in finance, investment operations and fund accounting & administration prior to joining Armstrong Asset Management. Jason joined from L Capital Asia where he spent 2 years in the capacity of VP Finance and Investor Reporting. Before crossing over to the buy side , Jason worked 5 years with Goldman Sachs Ireland and Citco Fund Services Ireland specializing in fund accounting and middle office operations for hedge funds and private equity funds. Prior to that, Jason was with Ernst & Young Ireland specializing in investment funds audit. Jason is a qualified Chartered Accountant (UK and Ireland), as well as a CFA charter holder. Jason received his Bachelor of Arts (majoring in Business and Finance) from Portobello College Dublin with first class honors.
                                        </div>
                                    </div>
                                </div>
                                <p>Jason Wang<br/>Financial Controller</p>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 tooltips">
                                <div class="cv-tooltip">
                                    <img width="90%" src="images/team/thet_lin_thu.jpg" >
                                    <div class="arrow right"></div>
                                    <div class="img-tooltip right">
                                        <div class="tooltip-content">
                                            Thet was previously a Clean Development Mechanism (CDM) consultant, advising clients to achieve carbon financing for renewable power projects in South East Asia. He led the successful carbon finance due diligence audit of two projects in Indonesia: a large scale hydropower project and an industrial energy efficiency project in Indonesia. He graduated from the National University of Singapore with a Bachelor's in Engineering during which he completed a 1 year internship at a biomass cleantech startup in Sweden.
                                        </div>
                                    </div>
                                </div>
                                <p>Thet Lin Thu<br/>Investment Analyst</p>
                            </div>
                        </div>
                        <!-- <div class="item-content" id="item-content-3">
                            <h1>EXPERT PANEL</h1>
                            <div class="col-sm-6 col-md-3 col-lg-3 tooltips">
                                <p>
                                    <img width="90%" src="images/experts/byron_askin.gif" title="A seasoned private equity investor, Byron brings a unique blend of investment and operational expertise gained over 25 years in international markets. Most recently, Byron led Al-Salam Bank’s Asia Pacific investment team, managing $300 million in assets.   Byron joined the Bank from JAFCO Asia where he led a team focused on South / Southeast Asia and Australia, after spearheading JAFCO’s Indian private equity business. Prior to JAFCO, Byron was instrumental in leading key acquisitions in India and China for the Whirlpool Corporation’s expansion into Asia. He is a member of the Singapore Institute of Directors and has served as Independent Director on the boards of several companies. Byron received his MBA and Bachelor’s degree in Industrial and Operations Engineering both from the University of Michigan in the United States.">
                                </p>
                                <p>Byron E. Askin</p>
                            </div>
                            <div class="col-sm-6 col-md-3 col-lg-3 tooltips">
                                <p>
                                    <img width="90%" src="images/experts/melissa_brown.gif" title="Melissa Brown has played a leading role in various Asian investment organisations focused on mainstream and sustainable investment strategies for public and private equity investors.  She is a Partner at Daobridge Capital, a China-focused private equity firm and a Director of Sustainalytics, a global ESG research provider.  Until recently, Melissa was a member of the United Nations Principles for Responsible Investment advisory council and an Executive Director of ASrIA, where she led a range of ESG research activities. Melissa was previously Managing Director at IDFC Capital, a Singapore-based private equity fund of funds focusing on emerging market investments.  She has 15 years experience in Asia analysing Asian investments for Barclays Capital, JPMorgan and Citigroup.

Melissa is currently a government-appointed council member of the Hong Kong Institute of CPAs, and a member of the Hong Kong Securities and Futures Commission Public Shareholders Group

Melisa holds an MBA from Yale School of Management and a BA in Economics from Williams College.">
                                </p>
                                <p>Melissa Brown</p>
                            </div>
                            <div class="col-sm-6 col-md-3 col-lg-3 tooltips">
                                <p>
                                    <img width="90%" src="images/experts/fong_wai_leong.gif" title="Wai Leong worked with Andrew Affleck at Devonshire Capital in Asia, which Wai Leong joined in 2002. His experience in finance and banking over the last 19 years includes structuring and managing principal investments, advising on a number of successful public listings as well as mergers and acquisitions in various countries, including Malaysia, China and Thailand. Prior to joining Devonshire Capital, he was the General Manager of Kuala Lumpur City Securities Sdn. Bhd., where he headed the firm’s Corporate Finance Division. He also worked at CIMB as a Manager in the Corporate Finance Division. Wai Leong is a Chartered Accountant with membership of the Malaysian Institute of Certified Public Accountants.">
                                </p>
                                <p>Fong Wai Leong</p>
                            </div>
                            <div class="col-sm-6 col-md-3 col-lg-3 tooltips">
                                <p>
                                    <img width="90%" src="images/experts/arun_sen.gif" title="Arun Sen is CEO of Lanco Power International, a company based in Singapore and focused on the development, ownership and operation of international power generation businesses globally. Arun has 19 years of global experience developing and investing in power and energy opportunities. Previously, he was Managing Director of Coromandel Advisors, a Singapore-based firm that advises corporate and private equity investors on LCT investments. In 2002, he co-founded Globeleq Ltd, a company that invested US$850 million in energy businesses in emerging markets in Asia, Latin America and Africa. Globeleq successfully exited its investments in 2007. Previously, Arun co-founded and was a principal Hart Energy International, an energy project development and investment company focused on the U.S. and Caribbean. From 1994 to 1999, he was with the Coastal Corporation, a Houston-based energy company, with responsibility for business development and asset management in South Asia. Arun began his career in the energy industry developing projects for environmental improvements in Eastern Europe in the early 1990s. He holds an M.S. in Finance from The American University in Washington, D.C.">
                                </p>
                                <p>Arun Sen</p>
                            </div>
                            <div class="col-sm-6 col-md-3 col-lg-3 tooltips">
                                <p>
                                    <img width="90%" src="images/experts/asgari.jpg" title="Asgari is a director and founding member of Intelligent Capital Sdn Bhd (Intelligent Capital) and an independent non-executive director of Mudajaya Group Berhad. He also serves as non executive director on the boards of JayCorp Berhad and Privasia Technology Berhad. He has extensive experience in both public and private equity investing in Malaysia. He has been involved in several start-up companies as an angel investor and has been actively involved in building their businesses as mentor. Several of these companies have gone public. He started his career working in general management in companies involved in a wide range of industries. He joined Usaha Tegas Sdn Bhd (UTSB) in 1988 where he worked in various capacities. He left in 1990 to join the stockbroking industry. He returned to UTSB in 1992 before leaving in 1995 to co-found Kumpulan Sentiasa Cemerlang Sdn Bhd (KSC), an investment advisory and fund management group. He took a year off to work with the National Economic Action Council (NEAC) in 1998. After his period at the NEAC, he started two venture capital firms, Intelligent Capital and iSpring Venture Management Sdn Bhd, while continuing to work with KSC. He was previously the chairman of the Malaysian Venture Capital Association.

He holds a Bachelor of Commerce (Honours) from the University of Melbourne in Australia and a Masters of Business Administration from Cranfield University in the UK.">
                                </p>
                                <p>Asgari bin Mohd Fuad Stephens</p>
                            </div>
                            <div class="col-sm-6 col-md-3 col-lg-3 tooltips">
                                <p>
                                    <img width="90%" src="images/experts/philippe_damas.jpg" title="Mr. Philippe G. J. E. O. Damas served as Chief Executive Officer of Retail and Banking Asia, ING Group (Singapore) from November 2008 to November 2009 and Retail Banking Asia, Global Private Banking & ING Trust, ING Group (Singapore) from 2005 to 2008. Mr. Damas serves as the Chairman and Member of Board of Executive Directors at TMB Bank Public Company Limited. He has been Vice Chairman of the Board at ING Vysya Bank Ltd since 2006. He has been an Independent Director of METROCOM Bank in Kazakhstan since Mar 2011. Mr. Damas has been a Director of ING Mauritius since 2006 and ING Asia Private Bank (Singapore) since 2001. He has been a Director of TMB Bank Public Co. Ltd. since December 28, 2007. Mr. Damas holds MBA from Columbia course University, U.S.A. in Accounting and International Finance. He studied at Military Service, Belgian Army; Advanced Automatics in Ecole Nationale de l’Aéronautique et de l’Espace, Toulouse, France and Ingénieur Civil Electricien Mécanicien, Université Libre de Bruxelles, Brussels, Belgium.">
                                </p>
                                <p>Philippe Damas</p>
                            </div>
                            <div class="col-sm-6 col-md-3 col-lg-3 tooltips">
                                <p>
                                    <img width="90%" src="images/experts/wiratman.jpg" title="Professor Wiratman Wangsadinata is the founder of one of the foremost engineering consultancies in Indonesia – PT Wiratman & Associates. Since the founding of PT Wiratman in 1976, Professor Wangsadinata’s firm has completed over 4100 multi-disciplinary consultancy works – including airports, highways, irrigation, commercial buildings, ports and power plants. In the power sector, PT Wiratman is recognized particularly for its hydropower consultancy expertise covering the full range of micro, mini and large scale hydropower plants. In his academic career, Professor Wangsadinata published over 200 technical papers, was a Professor of Structural Engineering at the Bandung Institute of Technology from 1995 to 2000 and is currently a Professor Emeritus at Tarumanagara University.">
                                </p>
                                <p>Wiratman Wangsadinata</p>
                            </div>
                        </div> -->

                        <span class="content-close" id="content-close" onclick="content_close()"></span>
                    </div>
                </div>
            <!-- </div> -->
            <!-- <div class="clearfix"></div> -->
        </div>

 
    

        <?php
            include ('footer.php');
        ?>

        <!-- Jquery -->
        <!-- // <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
        <script src="js/armstrongam.js"></script>
        <script>
            // $('img').popover({
            //     trigger: 'hover',
            //     container: 'body'
            // });
            $('.img-tooltip').mCustomScrollbar({
                theme:"minimal",
                autoDraggerLength: false,
                autoHideScrollbar: false,
                mouseWheel:{ scrollAmount: 50 }
            });
        </script>
        <!--<script type="text/javascript" src="js/jquery.tooltipster.min.js"></script>
        <script>
            $(document).ready(function() {
                $('img').tooltipster({
                    maxWidth: 300,
                    // position: 'right'
                });
            });
        </script>-->

    </body>
</html>
