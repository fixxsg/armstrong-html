<?php
	$page = "legal";
	include ('header.php');
?>
	
		<style>
		 	#wrap-container{
		 		padding: 0px 60px;
		 	}
		 	#wrap-sidebar {
				padding-left: 30px;
				padding-right: 30px;
		 	}
		</style>

		<div id="wrap-container" class="container legal-disclaimer">
			<div id="wrap-sidebar">
        <div class="item-content">
          <h1>COOKIES</h1>
          <p>A cookie is a small string of information that a website places in the cookie file of the browser on your computer's hard disk so that the website can remember you. A cookie will typically contain the name of the website the cookie has come from, a value and the ‘lifetime’ of the cookie.</p>
					<p>We will not use cookies to collect personally identifiable information about you as our cookies are there to simply help with your overall experience on our website. Our cookies help us to analyse how our customers use our website and enables basic personalisation of our site, which further improves your experience.</p>
					<p>We use several different types of cookies:</p>
					<p><strong>Session cookies: </strong>Temporary cookies which remain in your cookie file until you leave our website.</p>
					<p><strong>Persistent cookies:</strong> These remain in place across multiple visits to our sites.</p>
					<p><strong>Third Party Cookies:</strong> These are created by a third party to provide us with statistics. An example of this is Google analytics.</p>
					<p><strong>Consent to use of cookies</strong></p>
					<p>By using our website you consent to our use of cookies to process the information we gather.</p>
					<p><strong>Disabling Cookies</strong></p>
					<p>You can choose to accept or decline cookies by changing the settings in your browser. More information on how to change your browser settings and deleting cookies can be found here at <a href="http://www.aboutcookies.org" mce_href="http://www.aboutcookies.org" target="_blank">aboutcookies.org</a>.</p>
        </div>
      </div>
      <div class="clearfix"></div>
  	</div>

<?php
	include ('footer.php');
?>

<!-- script -->
<script src="js/armstrongam.js"></script>