<?php
	$page = "legal";
	include ('header.php');
?>
		<style>
		 	#wrap-container{
		 		padding: 0px 60px;
		 	}
		 	#wrap-sidebar {
				padding-left: 30px;
				padding-right: 30px;
		 	}
		</style>

		<div id="wrap-container" class="container legal-disclaimer">
			<div id="wrap-sidebar" style="margin:0">
				<div class="item-content">
				  <h1>LEGAL DISCLAIMER</h1>
				  <p style="text-align: justify;">The contents of this website have been prepared by Armstrong Asset Management Pte Ltd (AAMPL) and are made available for information purposes only. The information herein has not been independently verified and no guarantee, representation or warranty, express or implied, is made as to its accuracy, completeness or correctness. Opinions expressed herein are subject to change without notice. AAMPL undertakes no obligation to update the contents herein or to correct any inaccuracies which may become apparent.</p>
					<p style="text-align: justify;">Past performance is not indicative of future or likely performance.</p>
					<p style="text-align: justify;">Nothing herein should be construed as an offer, invitation or solicitation to deal in any securities in any jurisdiction. All views expressed herein do not have regard to the specific investment objectives, financial situation and particular needs of any person who may read the contents herein and cannot be construed as recommendation or investment advice.</p>
					<p style="text-align: justify;">Nothing herein should be considered to constitute investment, legal, accounting or taxation advice and you must independently determine, with your own advisers, the appropriateness for you of the financial instruments and/or transactions contemplated herein. You must not rely on any information contained herein. AAMPL, its affiliates and the individuals associated therewith may (in various capacities) have positions or deal in financial instruments and transactions (or related derivatives) identical or similar to those described herein.</p>
					<p style="text-align: justify;">AAMPL is not responsible for the content of any third-party website or any linked content contained in a third-party website. Content contained on such third-party websites is not part of materials prepared by AAMPL and are not incorporated by reference herein. The inclusion of a link herein does not imply any endorsement by or any affiliation with AAMPL. Access to any third-party website is at your own risk and you should always review the terms and privacy policies at third-party websites before submitting any personal information to them. AAMPL is not responsible for such terms and privacy policies and expressly disclaims any liability for them.</p>
					<p style="text-align: justify;">AAMPL is a limited liability private company incorporated in Singapore with registration number 200819646R and registered address at 30 Raffles Place #10-04 Chevron House, Singapore 048622. AAMPL has submitted to the Monetary Authority of Singapore and has received acknowledgement of its notification as an exempt fund manager pursuant to paragraph 5(1)(d) of the Second Schedule to the Securities and Futures (Licensing and Conduct of Business) Regulations (Rg 10).</p>
				</div>
	    	</div>
	   	</div>

<?php
	include ('footer.php');
?>

 	<!-- Jquery -->
  <script src="js/armstrongam.js"></script>
